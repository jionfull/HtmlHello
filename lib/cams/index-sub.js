// 首页index.html对应js

/**
 * 定义三维地图上的所亭对象以及显示设置
 */
let SubstationEntity = function() {
    let _this = this;
    this.substations = {};
    let _pinBuilder = new Cesium.PinBuilder();
    let _colorDef = {
        alert: Cesium.Color.RED,
        defect: Cesium.Color.YELLOW
    };

    function createSubHtml(data) {
        // 状态颜色
        let statusClass = ["tx_gray01", "bg_gray"];
        let navToSubMenu = "";
        let countText = "";

        if (data.online) {
            if (data.alarmCount && +data.alarmCount) {
                statusClass = ["tx_red", "bg_red"];
                countText = '<em>' + data.alarmCount + '</em>';
            } else if (data.defectCount && +data.defectCount) {
                statusClass = ["tx_orange", "bg_orange"];
                countText = '<em>' + data.defectCount + '</em>';
            } else {
                statusClass = ["tx_green", "bg_green"];
            }
        }

        // 正在巡检。
        let onInspectionClass = data.onInspection ? "g_rotating" : "";

        let subIcon = "";
        // SUBTYPE_CODE:31	变电所
        // SUBTYPE_CODE:32	配电所
        // SUBTYPE_CODE:33	车间变电所
        // SUBTYPE_CODE:34	开闭所
        // SUBTYPE_CODE:35	箱变
        // SUBTYPE_CODE:36	AT所
        // SUBTYPE_CODE:37	分区所
        if (data.subtypeCode == 'SUBTYPE_CODE:31') {
            subIcon = "iconfont icon-e_biand";
        } else if (data.subtypeCode == 'SUBTYPE_CODE:34') {
            subIcon = "iconfont icon-e_kaibs";
        } else if (data.subtypeCode == 'SUBTYPE_CODE:36') {
            subIcon = "iconfont icon-e_ats";
        } else if (data.subtypeCode == 'SUBTYPE_CODE:37') {
            subIcon = "iconfont icon-e_fengqs";
        }

        data.shortSubName = data.subName
            .replace('牵引变电所', '')
            .replace('变电所', '')
            .replace('AT所兼开闭所', '')
            .replace('AT兼开闭所', '')
            .replace('AT开闭所', '')
            .replace('AT所兼分区所', '')
            .replace('AT兼分区所', '')
            .replace('AT分区所', '')
            .replace('AT所', '')
            .replace('分区所兼开闭所', '')
            .replace('分区兼开闭所', '')
            .replace('分区开闭所', '')
            .replace('分区所', '')
            .replace('开闭所', '');

        let html =
            '<a onclick="toSubMenu(\'' + (data.online ? data.subId : "") + '\');" class="gis_coordin ' + statusClass[0] + '" style="">' +
            '    <i class="' + subIcon + ' ' + statusClass[1] + ' ' + onInspectionClass + '"></i>' +
            '    <span>' + data.shortSubName + countText + '</span>' +
            '</a>';

        return html;
    }

    this.init = function(callback) {
        $.get(ctx + "/cams/plane/getLineSubAll", function(result) {
            if (result && result.status == 0) {
                for (let i = 0, ii = result.data.length; i < ii; i++) {
                    for (let j = 0, jj = result.data[i].substations.length; j < jj; j++) {
                        _this.addEntity(result.data[i].substations[j])
                    }
                }
            }

            if (callback) {
                callback();
            }
        });
    };

    this.addEntity = function(substation) {
        let subId = substation.subId;
        if (!this.substations[subId] && substation.geom) {
            let pointStrs = substation.geom.split(/[(,), ]/);
            substation.position = {
                x: +pointStrs[1],
                y: +pointStrs[2]
            };
            let overlay = new Cesium.gm.Overlay({
                id: subId,
                viewer: viewer,
                position: new Cesium.Cartesian3.fromDegrees(substation.position.x, substation.position.y, 0),
                offset: [-13, -38],
                content: createSubHtml(substation)
            });

            this.substations[subId] = substation;
        }
    };

    /**
     * 告警
     */
    this.alertMark = function(subId, count) {
        let substation = this.substations[subId];
        if (count != substation.alarmCount) {
            substation.alarmCount = count;
            let content = createSubHtml(substation);
            Cesium.gm.Overlay.getItem(subId).setContent(content);
        }
    };

    /**
     * 缺陷
     */
    this.defectMark = function(subId, count) {
        let substation = this.substations[subId];
        if (count != substation.defectCount) {
            substation.defectCount = count;
            let content = createSubHtml(substation);
            Cesium.gm.Overlay.getItem(subId).setContent(content);
        }
    };

    /**
     * 设置巡检状态
     * @param onInspection
     */
    this.setInspection = function(subId, onInspection) {
        let substation = this.substations[subId];
        if (!!substation.onInspection != !!onInspection) {
            substation.onInspection = onInspection;
            let content = createSubHtml(substation);
            Cesium.gm.Overlay.getItem(subId).setContent(content);
        }
    }
};