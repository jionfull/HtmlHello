// 首页index.html对应js

let viewer = undefined;
let nameOverlay = undefined;
let substationEntity = undefined;

/**
 * 地图默认视角
 */
let homeViewParam = undefined;

/**
 * 初始化地图。
 */
function initMap() {
    camsGisConfigInfo.d3View = Cesium.gm.Tools.toJson(camsGisConfigInfo.d3View);
    homeViewParam = {
        destination: Cesium.Cartesian3.fromDegrees(
            camsGisConfigInfo.d3View[0][0],
            camsGisConfigInfo.d3View[0][1],
            camsGisConfigInfo.d3View[0][2]),
        orientation: {
            heading: Cesium.Math.toRadians(camsGisConfigInfo.d3View[1][0]),
            pitch: Cesium.Math.toRadians(camsGisConfigInfo.d3View[1][1]),
            roll: Cesium.Math.toRadians(camsGisConfigInfo.d3View[1][2])
        }
    };

    let url = camsGisConfigInfo.gisSzgdUrlTile;
    url = url.replace('{0}', '{x}');
    url = url.replace('{1}', '{y}');
    url = url.replace('{2}', '{z}');
    url = url.replace('{z}', '{z1}');
    let gmImageryProvider = new Cesium.UrlTemplateImageryProvider({
        url: url,
        tilingScheme: new Cesium.GeographicTilingScheme(),
        customTags: {
            z1: function(imageryProvider, x, y, level) {
                return level + 1
            }
        }
    });

    viewer = new Cesium.Viewer('cesiumContainer', {
        imageryProvider: gmImageryProvider,
        contextOptions: {
            webgl: {
                alpha: true
            }
        },
        sceneMode: Cesium.SceneMode.SCENE3D,
        selectionIndicator: false,
        animation: false,
        baseLayerPicker: false,
        geocoder: false,
        timeline: false,
        sceneModePicker: false,
        homeButton: false,
        navigationHelpButton: false,
        infoBox: false,
        fullscreenButton: false,
        vrButton: false,
        shadows: false
    });

    // viewer.scene.globe.depthTestAgainstTerrain = true;
    // 取消默认logo显示
    viewer._cesiumWidget._creditContainer.style.display = "none";

    viewer.camera.setView({
        destination: homeViewParam.destination,
        orientation: homeViewParam.orientation
    });

    // 添加区间线路
    viewer.imageryLayers.addImageryProvider(Cesium.gm.createGeoServerWmtsLayer({
        url: camsGisConfigInfo.gisSzgdUrlWmts,
        layer: 'SZGD:MYSQL_GEO_ST_LINE'
    }));
    // 添加站点
    viewer.imageryLayers.addImageryProvider(Cesium.gm.createGeoServerWmtsLayer({
        url: camsGisConfigInfo.gisSzgdUrlWmts,
        layer: 'SZGD:MYSQL_GEO_ST_SITE'
    }));
    // 设置后当相机高度达到设置的最大和最小高度时将不再放大和缩小
    viewer.scene.screenSpaceCameraController.minimumZoomDistance = 800; //相机的高度的最小值
    viewer.scene.screenSpaceCameraController.maximumZoomDistance = 1200000; //相机高度的最大值

    // viewer.camera.setView({
    //     destination: new Cesium.Cartesian3(-8445564.183351874, 31020147.26548122, 22614220.439627644),
    //     orientation: {
    //         heading: 6.283185307179586,
    //         pitch: -1.5685984592333182,
    //         roll: 0
    //     }
    // });

    // viewer.camera.flyTo({
    //     destination : homeViewParam.destination,
    //     orientation: homeViewParam.orientation,
    //     complete: function () {
    //         // 添加区间线路
    //         viewer.imageryLayers.addImageryProvider(Cesium.gm.createGeoServerWmtsLayer({
    //             url: camsGisConfigInfo.gisSzgdUrlWmts,
    //             layer: 'SZGD:MYSQL_GEO_ST_LINE'
    //         }));
    //         // 添加站点
    //         viewer.imageryLayers.addImageryProvider(Cesium.gm.createGeoServerWmtsLayer({
    //             url: camsGisConfigInfo.gisSzgdUrlWmts, layer: 'SZGD:MYSQL_GEO_ST_SITE'
    //         }));
    //         // 添加所亭
    //         // viewer.imageryLayers.addImageryProvider(Cesium.gm.createGeoServerWmtsLayer(
    //         //     camsGisConfigInfo.gisSzgdUrlWmts, 'SZGD:MYSQL_GEO_SUBSTATION'));
    //
    // //         // 地图视图设置。
    // //         // viewer.scene.screenSpaceCameraController.enableZoom = false;
    // // //        viewer.scene.screenSpaceCameraController.enableTilt = false;
    // // //        Cesium.gm.showMousePointCoord(viewer, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    // //         // viewer.scene.highDynamicRange=false;
    // //         // viewer.scene.globe.enableLighting = true
    // //
    //         // 设置后当相机高度达到设置的最大和最小高度时将不再放大和缩小
    //         viewer.scene.screenSpaceCameraController.minimumZoomDistance = 1000;//相机的高度的最小值
    //         viewer.scene.screenSpaceCameraController.maximumZoomDistance = 1200000;  //相机高度的最大值
    //     }
    // });

    // hoverMap.init(viewer);
    // hoverMap.beginCheck();
    nameOverlay = new Cesium.gm.NameOverlay(viewer);

    substationEntity = new SubstationEntity();
    substationEntity.init(function() {
        setTimeout(function() {
            refreshAlarmCount();
            initStatistics();
            refreshInspection();
            refreshInspectionAbnormal();
        }, 1500);

        // 刷新报警数量显示。
        setInterval(function() {
            refreshAlarmCount();
            initStatistics();
        }, 5000);

        // 更新巡检状态显示。
        setInterval(function() {
            refreshInspection();
            refreshInspectionAbnormal();
        }, 10000);
    });
}

/**
 * 更新报警和缺陷数量
 */
function refreshAlarmCount() {
    // 报警数量
    $.get(ctx + '/cams/realTimeAlarm/getSubAlarmCount', function(result) {
        if (result && result.status == 0) {
            for (let subId in substationEntity.substations) {
                let count = result.data[subId] || 0;
                substationEntity.alertMark(subId, count);
            }
        }

        // 缺陷数量
        $.get(ctx + '/cams/patrolDefect/findCount', function(result) {
            if (result && result.status == 0) {
                for (let subId in substationEntity.substations) {
                    let count = result.data[subId] || 0;
                    substationEntity.defectMark(subId, count);
                }
            }
        });
    });
}

/**
 * 更新巡检状态
 */
function refreshInspection() {
    $.get(ctx + '/cams/plane/getOnInspectionSubList', function(result) {
        if (result && result.status == 0) {
            for (let subId in substationEntity.substations) {
                let substation = substationEntity.substations[subId];
                let onInspection = false;

                for (let i = 0, ii = result.data.length; i < ii; i++) {
                    if (result.data[i].subId == subId) {
                        onInspection = true;
                        break;
                    }
                }

                substationEntity.setInspection(substation.subId, onInspection);
            }

            $('#inspectionTable').datagrid('loadData', result.data);
            $('#inspectionCount').html(result.data.length);
        }
    });
}

/**
 * 更新巡检异常
 */
function refreshInspectionAbnormal() {
    $.get(ctx + '/cams/plane/getInspectionAbnormalList', function(result) {
        if (result && result.status == 0) {
            $('#inspectionAbnormalTable').datagrid('loadData', result.data);
            $('#inspectionAbnormalCount').html(result.data.length);
        }
    });
}

/**
 * 恢复到默认视图
 */
function toDefaultView() {
    viewer.camera.flyTo({
        destination: homeViewParam.destination,
        orientation: homeViewParam.orientation
    });
}

/**
 * WMS查询
 */
var queryWms = function(position, callback) {

    var url = camsGisConfigInfo.gisSzgdUrlWms + "?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo" +
        "&QUERY_LAYERS=SZGD%3AMYSQL_GEO_SUBSTATION&LAYERS=SZGD%3AMYSQL_GEO_SUBSTATION&INFO_FORMAT=application%2Fjson&" +
        "X=50&Y=50&SRS=EPSG%3A4326&WIDTH=101&HEIGHT=101";

    var ray = viewer.camera.getPickRay(position);
    var cartesian = viewer.scene.globe.pick(ray, viewer.scene);
    if (cartesian) {
        var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
        var coordX = Cesium.Math.toDegrees(cartographic.longitude);
        var coordY = Cesium.Math.toDegrees(cartographic.latitude);

        var cameraCartographic = Cesium.Cartographic.fromCartesian(viewer.camera.position);
        var raidus = cameraCartographic.height / 2000000.0;

        url += "&BBOX=" + (coordX - raidus) + "," + (coordY - raidus) + "," + (coordX + raidus) + "," + (coordY + raidus)
    }

    Cesium.gm.query.request(url, null, function(data) {
        if (data) {
            if (data.features && data.features.length > 0) {
                if (callback) {
                    callback(data.features[0].properties.SUB_ID);
                    return;
                }
            }
        }

        callback(undefined);
    });
};