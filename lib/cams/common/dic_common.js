/*
 * 此js 主要用于对字典的统一转换
 * */

//报警级别映射关系
var ALAM_LEVEL = [{
        'code': '01',
        'value': 'I级'
    },
    {
        'code': '02',
        'value': 'II级'
    },
    {
        'code': '03',
        'value': 'III级'
    },
    {
        'code': '04',
        'value': 'IV级'
    },
    {
        'code': '05',
        'value': 'V级'
    },
    {
        'code': '06',
        'value': 'VI级'
    },
    {
        'code': '07',
        'value': 'VII级'
    }
];
//报警管理报警及事件类型 映射关系
var CAMS_ALAM_TYPE = [{
        'code': '01',
        'value': '模拟量越限'
    },
    {
        'code': '02',
        'value': '数字量变位'
    },
    {
        'code': '03',
        'value': '设备故障'
    },
    {
        'code': '04',
        'value': '通信故障'
    }
];
//报警管理数据点
var ALARM_DATATYPE = [{
        'code': '01',
        'value': '告警'
    },
    {
        'code': '02',
        'value': '温度'
    },
    {
        'code': '03',
        'value': '故障状态'
    },
    {
        'code': '04',
        'value': '通信状态'
    },
    {
        'code': '05',
        'value': '分合状态'
    },
    {
        'code': '06',
        'value': '操作机构松脱'
    },
    {
        'code': '07',
        'value': '油位'
    }
];
var cams_alarm_sourcetype = [{
        'code': '01',
        'value': '视频监控'
    },
    {
        'code': '02',
        'value': '安全防范'
    },
    {
        'code': '03',
        'value': '门禁'
    },
    {
        'code': '04',
        'value': '火灾报警'
    },
    {
        'code': '05',
        'value': '环境监测'
    },
    {
        'code': '06',
        'value': '供电设备'
    },
    {
        'code': '07',
        'value': '暂无'
    }
];

//报警管理数据类型
var CAMS_ALAM_DATACLASS = [{
        'code': '01',
        'value': '遥信'
    },
    {
        'code': '02',
        'value': '遥测'
    },
    {
        'code': '03',
        'value': '遥控'
    },
    {
        'code': '04',
        'value': '遥调'
    }
];
//缺陷状态 
var DEFECT_STATUS = [{
    'code': '1',
    'value': '新登记'
}, {
    'code': '2',
    'value': '处理中'
}, {
    'code': '3',
    'value': '已处理'
}, {
    'code': '4',
    'value': '已销号'
}];

//缺陷等级
var DEFECT_QXDJ = [{
        'code': '1',
        'value': '一级缺陷'
    },
    {
        'code': '2',
        'value': '二级缺陷'
    },
    {
        'code': '3',
        'value': '三级缺陷'
    }
];

//实时报警状态
var ALARM_STATUS = [{
        'code': '0',
        'value': '未确认'
    },
    {
        'code': '1',
        'value': '已确认'
    }
];
//联动记录状态
var ALARM_LINK_STATUS = [{
        'code': '1',
        'value': '已执行'
    },
    {
        'code': '2',
        'value': '执行失败'
    }
];


/*根据code 和  type （CAMS_ALAM_TYPE color 字体颜色)*/
function getValueByCodeAndType(code, type, color) {
    var value = "";
    if (type.length > 0) {
        for (var i = 0; i < type.length; i++) {
            var obj = type[i];
            if (obj.code == code) {
                value = obj.value;
                if (color) {
                    return '<span style="color:' + color + '">' + value + '</span>';
                } else {
                    return value;
                }
            }
        }
    }
    return value;
}