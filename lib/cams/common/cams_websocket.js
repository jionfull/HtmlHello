var _socket = {};
_socket.tempChecked = null;
_socket.gridContain = function(tragetIds, gridCom, type) {
    var isContain = false;
    if (type != null && type != '') { //如果当页面类型与当前要刷新的类型不一致，则不进行刷新
        if (tragetIds.indexOf(type) == -1) {
            isContain = false;
            return isContain;
        }
    }

    isContain = true;

    /*if(tragetIds.indexOf('@')!= -1){//证明是新建
    	isContain = true;
    }else{
    	var tempGrid = null
    	if(gridCom){
    		tempGrid = gridCom;
    	}else{
    		tempGrid = $('#dg');
    	}
    	
    	var rows = tempGrid.datagrid('getRows');
    	for(var i=0;i<rows.length;i++){
    		var row = rows[i];
    		if(tragetIds.indexOf(row.id) != -1){
    			isContain = true;
    			break;
    		}
    	}
    }*/
    return isContain;
}

/**
 * grid加载完成后，重新选择勾选框
 */
_socket.onLoadSucessCheck = function() {
    //刷新列表后重新勾选对应行记录
    if (_socket.tempChecked != null) {
        var chekckedMap = {};
        for (var i = 0; i < _socket.tempChecked.length; i++) {
            chekckedMap[_socket.tempChecked[i].id] = true;
        }

        var tempM = [];
        var rows = $('#dg').datagrid('getRows');


        for (var i = 0; i < rows.length; i++) {
            if (chekckedMap[rows[i].id] != null) {
                tempM.push(i);
            }
        }
        for (var i = 0; i < tempM.length; i++) {
            $('#dg').datagrid('checkRow', tempM[i]);
        }
    }

}
/**
 * 接收系统消息推送，用于刷新列表数据
 * 在需要系统推送刷新表格的页面加载完成事件中调用此方法
 * 需要刷新的表格ID请使用dg开头，例如：dg，dg_cmd
 */
function receivePushUpTable(type) {
    receivePushUpData(function(data) {
        $("table[id^='dg']").each(
            function() {
                if ($(this).attr("class")) {
                    var options = $(this).datagrid('options');
                    if (options.treeField != null && options.treeField != '') { //判断是不是treegrid
                        $(this).treegrid('reload');
                    } else {
                        if (data) {
                            var isContain = _socket.gridContain(data, $(this), type);
                            if (!isContain) {
                                return;
                            }
                        }
                        _socket.tempChecked = $(this).datagrid('getChecked');
                        $(this).datagrid('reload');
                    }
                }
            }
        );
    });
}

/**
 * 接收系统消息推送，用于刷新页面数据
 * 在需要系统推送刷新数据的页面加载完成事件中调用此方法
 */
function receivePushUpData(callback) {
    var socket = new SockJS(ctx + '/stomp');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        stompClient.subscribe('/topic/server/update', function(data) {
            console.log("存在刷新任务 =" + data.body);
            callback(data.body);
        });
    });
}