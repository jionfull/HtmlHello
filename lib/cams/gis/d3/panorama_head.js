/**
 * 全景监控头部工具条公用方法。
 */
let panoramaHead = new function() {
    let _this = this;
    let _onSubListChangeEvents = [];
    let _onZoneChangeEvents = [];

    let _startPosition = undefined;
    let _endPosition = undefined;
    let _sceneControlInit = false;

    this.lineId = undefined;
    this.subId = undefined;
    this.currentZoneCode = undefined;
    this.viewer = undefined;
    this.subModel = undefined;
    this.firstSetSub = true;

    /**
     * @param opt 初始化参数.
     * @param opt.subId
     * @param opt.viewer {Cesium.View|function}
     * @param opt.subModel {Cesium.View|function}
     * @param opt.onSubListChanged {function}
     * @param opt.onZoneChanged {function}     *
     */
    this.init = function(opt) {
        _this.lineId = opt.lineId;
        _this.subId = opt.subId;
        _this.viewer = opt.viewer;
        _this.subModel = opt.subModel;

        initArea();
        initLineList();
        // initSubList({"id":lineId});
        initSceneMode();
        initTransCheckBox();

        _onSubListChangeEvents = [];
        _this.onSubListChanged(opt.onSubListChanged);

        _onZoneChangeEvents = [];
        _this.onZoneChanged(opt.onZoneChanged);
    };
    this.initAllSubList = function() {
        initAllSubList();
    };
    /**
     * 添加所亭下拉事件
     * @param callback {Function} 当重新选择所亭后调用的方法，参数subId。
     */
    this.onSubListChanged = function(callback) {
        if (callback) {
            _onSubListChangeEvents.push(callback);
        }
    };

    /**
     * 添加区域切换事件
     * @param callback {Function} 当重点击区域按钮调用的方法，参数zoneCode。
     */
    this.onZoneChanged = function(callback) {
        if (callback) {
            _onZoneChangeEvents.push(callback);
        }
    };

    //初始化区域，并绑定事件
    function initArea() {
        $.get(ctx + "/cams/gis3d/listEquipArea", function(data) {
            $("#ulTabs").empty();
            let liHtml = '';
            let liClass = "";
            for (let i = 0, ii = data.length; i < ii; i++) {
                if (i === 0) {
                    liClass = "tabs-first tabs-selected";
                } else if (i === (ii - 1)) {
                    liClass = "tabs-last";
                } else {
                    liClass = 'tabs-inner';
                }

                liHtml += '<li id="' + data[i].code + '" class="' + liClass + '"><a class="tabs-inner">' + data[i].name + '</a></li>';
            }

            $("#ulTabs").append(liHtml);
            $('#ulTabs li').click(function() {
                $(this).siblings('li').removeClass('tabs-selected');
                $(this).addClass('tabs-selected');
                let name = $($(this).find("a")[0]).html();
                $("#tit").panel("setTitle", name + "缺陷及报警");
                // 点击区域按钮。
                _this.currentZoneCode = $(this).attr("id");
                for (let i = 0; i < _onZoneChangeEvents.length; i++) {
                    _onZoneChangeEvents[i](_this.currentZoneCode);
                }
            });
        });
    }

    //初始化线路列表
    function initLineList() {
        $('#lineList').combobox({
            url: ctx + '/cams/org/line',
            valueField: 'id',
            textField: 'text',
            width: 160,
            panelHeight: 200,
            prompt: '选择线路...',
            onSelect: initSubList,
            onLoadSuccess: function(e) {
                if (lineId) {
                    $('#lineList').combobox("setValue", lineId);
                }
            },
            onShowPanel: function() {
                hideWnd();
            },
            onHidePanel: function() {
                try {
                    showWnd()
                } catch (e) {}
            },
        });
    }

    // 初始化所亭列表
    function initSubList(res) {
        $('#subList').combobox({
            //url: ctx + '/cams/gis3d/getOnlineSubList',
            url: ctx + '/cams/org/substation?lineId=' + res.id,
            valueField: 'id',
            width: 160,
            panelHeight: 200,
            prompt: '选择所亭...',
            required: true,
            textField: 'text',
            onLoadSuccess: function(e) {
                if (e.length === 0) {
                    Msg.info('没有所亭');
                    return true;
                }

                for (var i = 0; i < e.length; i++) {
                    if (e[i].id == subId) {
                        $('#subList').combobox("setValue", subId);
                        return true;
                    }
                }
                $('#subList').combobox("setValue", e[0].id);
            },
            onShowPanel: function() {
                hideWnd();
            },
            onHidePanel: function() {
                try {
                    showWnd()
                } catch (e) {}
            },
            onSelect: function(record) {
                if (record.id == _this.subId) {
                    return;
                }
                // if(firstSetSub){
                // 	firstSetSub=false;
                // 	return;
                // }
                // 选择不同的所亭。                
                $.get(ctx + "/cams/choiceSubstation?subId=" + record.id, function(result) {
                    if (result && result.status == 0) {
                        _this.subId = record.id;

                        for (let i = 0; i < _onSubListChangeEvents.length; i++) {
                            _onSubListChangeEvents[i](_this.subId);
                        }
                    }
                });
            }
        });
    }
    //查询所有变电所列表
    function initAllSubList() {
        $('#subList').combobox({
            url: ctx + '/cams/gis3d/getOnlineSubList',
            valueField: 'id',
            width: 160,
            panelHeight: 200,
            prompt: '选择所亭...',
            required: true,
            textField: 'subname',
            onLoadSuccess: function(e) {
                if (e.length === 0) {
                    Msg.info('没有所亭');
                    return true;
                }
                for (var i = 0; i < e.length; i++) {
                    if (e[i].id == subId) {
                        $('#subList').combobox("setValue", subId);
                        return true;
                    }
                }
                $('#subList').combobox("setValue", e[0].id);
            },
            onShowPanel: function() {
                try {
                    hideWnd();
                } catch (err) {}
            },
            onHidePanel: function() {
                try {
                    showWnd()
                } catch (e) {}
            },
            onSelect: function(record) {
                if (record.id == _this.subId) {
                    return;
                }
                // 选择不同的所亭。                
                $.get(ctx + "/cams/choiceSubstation?subId=" + record.id, function(result) {
                    if (result && result.status == 0) {
                        _this.subId = record.id;

                        for (let i = 0; i < _onSubListChangeEvents.length; i++) {
                            _onSubListChangeEvents[i](_this.subId);
                        }
                    }
                });
            }
        });
    }

    function initTransCheckBox() {
        $('#modelTrans').checkbox({
            label: '房屋透视',
            checked: true,
            labelPosition: 'after',
            onChange: function(checked) {
                let subModel = (typeof _this.subModel === 'function') ? _this.subModel() : _this.subModel;
                if (subModel) {
                    subModel.setAutoHideOnClose(checked);
                }
            }
        })
    }

    /**
     * 2.5D和3D模式切换。
     */
    function initSceneMode() {
        $('#viewModel').combobox({
            onSelect: function(record) {
                let viewer = (typeof _this.viewer === 'function') ? _this.viewer() : _this.viewer;
                let subModel = (typeof _this.subModel === 'function') ? _this.subModel() : _this.subModel;

                if (!viewer) {
                    return;
                }

                if (record.value === '2.5D') {
                    if (!_sceneControlInit) {
                        _sceneControlInit = addEventHandler();
                        if (!_sceneControlInit) {
                            return;
                        }
                    }

                    viewer.camera.lookAt(
                        subModel.tileset.boundingSphere.center,
                        new Cesium.HeadingPitchRange(+subModel.d3ModelProp.viewParam['00'].orientation.heading, -Math.PI / 6, 100));
                    viewer.camera.lookAtTransform(Cesium.Matrix4.IDENTITY);
                    viewer.scene.screenSpaceCameraController.enableTilt = false;
                    $('#ulTabs-panel').hide();
                } else {
                    viewer.scene.screenSpaceCameraController.enableTilt = true;
                    $('#ulTabs-panel').show();
                }
            }
        });

        /**
         * 注册鼠标事件。
         * @return {boolean} 注册成功
         */
        function addEventHandler() {
            let viewer = (typeof _this.viewer === 'function') ? _this.viewer() : _this.viewer;
            if (!viewer) {
                return false;
            }

            viewer.screenSpaceEventHandler.setInputAction(function(click) {
                _startPosition = click.position;

            }, Cesium.ScreenSpaceEventType.RIGHT_DOWN);

            viewer.screenSpaceEventHandler.setInputAction(function(click) {
                if (viewer.scene.screenSpaceCameraController.enableTilt === true ||
                    !_startPosition) {
                    return;
                }

                _endPosition = click.endPosition;

                let dragLength = 150;
                if (_endPosition.x - _startPosition.x > dragLength) {
                    // 向右拖拽
                    let pick = new Cesium.Cartesian2(viewer.scene.canvas.width / 2, viewer.scene.canvas.height / 2);
                    let position = viewer.scene.globe.pick(viewer.camera.getPickRay(pick), viewer.scene);
                    viewer.camera.rotate(position, Math.PI / 2);
                    _startPosition.x += dragLength;
                } else if (_endPosition.x - _startPosition.x < -dragLength) {
                    // 向左拖拽
                    let pick = new Cesium.Cartesian2(viewer.scene.canvas.width / 2, viewer.scene.canvas.height / 2);
                    let position = viewer.scene.globe.pick(viewer.camera.getPickRay(pick), viewer.scene);
                    viewer.camera.rotate(position, -Math.PI / 2);
                    _startPosition.x -= dragLength;
                }
            }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

            viewer.screenSpaceEventHandler.setInputAction(function() {
                _startPosition = null;
            }, Cesium.ScreenSpaceEventType.RIGHT_UP);

            return true
        }
    }


};