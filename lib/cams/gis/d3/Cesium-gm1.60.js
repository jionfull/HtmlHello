/**
 * 提供对于Cesium的常用方法的二次封装。
 * 基于Cesium1.60
 */
Cesium.gm = Cesium.gm || {};

/**
 * 创建默认通用地图
 * @param opt
 * @param.target {String} 绑定div的ID
 * @param.imgUrl 地图url
 * @return Cesium.Viewer
 */
Cesium.gm.createDefaultMap = function(opt) {

    let url = opt.imgUrl;
    url = url.replace('{0}', '{x}');
    url = url.replace('{1}', '{y}');
    url = url.replace('{2}', '{z}');
    url = url.replace('{z}', '{z1}');

    // 验证是否使用本地文件服务。
    if (opt.camsGisConfigInfo &&
        opt.camsGisConfigInfo.localServerEnable &&
        opt.camsGisConfigInfo.localServerEnable.toUpperCase() === "TRUE" &&
        opt.camsGisConfigInfo.localServerUrl) {
        let localServerUrl = opt.camsGisConfigInfo.localServerUrl;
        let healthUrl = localServerUrl + 'health';
        // if (Cesium.gm.Tools.netPing(healthUrl, 1000)) {
        //     $.post(localServerUrl + "setProxyBaseUrl/", {
        //         type: 'gisTile',
        //         baseUrl: url.substr(0, url.indexOf('/', 8))
        //     });
        //     url =  localServerUrl + "proxy/gisTile" + url.substr(url.indexOf('/' , 8));
        // }
    }

    let gmImageryProvider = new Cesium.UrlTemplateImageryProvider({
        url: url,
        tilingScheme: new Cesium.GeographicTilingScheme(),
        maximumLevel: 19,
        customTags: {
            z1: function(imageryProvider, x, y, level) {
                return level + 1
            }
        }
    });
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(85, 22, 122, 45);
    let viewer = new Cesium.Viewer(opt.target, {
        imageryProvider: gmImageryProvider,
        contextOptions: {
            webgl: {
                alpha: true
            }
        },
        skyBox: new Cesium.SkyBox({
            show: false
        }),
        selectionIndicator: false,
        animation: false,
        baseLayerPicker: false,
        geocoder: false,
        timeline: true,
        homeButton: false,
        sceneModePicker: false,
        navigationHelpButton: false,
        infoBox: false,
        fullscreenButton: false,
        vrButton: false,
        shadows: false
    });

    viewer.scene.globe.depthTestAgainstTerrain = true;
    viewer.scene.logarithmicDepthBuffer = false;
    // viewer.scene.sunColor.x = 8.8;
    viewer._cesiumWidget._creditContainer.style.display = "none";
    // viewer.extend(Cesium.viewerCesiumInspectorMixin);
    //viewer.showMousePointCoord(Cesium.ScreenSpaceEventType.LEFT_CLICK);

    //取消双击事件
    viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
    // 鼠标右键旋转视角。
    viewer.scene.screenSpaceCameraController.tiltEventTypes = [Cesium.CameraEventType.MIDDLE_DRAG, Cesium.CameraEventType.RIGHT_DRAG];
    viewer.scene.screenSpaceCameraController.zoomEventTypes = [Cesium.CameraEventType.WHEEL, Cesium.CameraEventType.PINCH];

    if (opt.wheelIntoModel !== false) {
        Cesium.gm.wheelIntoModel(viewer);
    }

    if (opt.addKeyNavigate !== false) {
        Cesium.gm.addKeyNavigate(viewer);
    }

    viewer.clock.startTime = Cesium.JulianDate.fromDate(new Date("2000-01-01 12:00:00"));
    viewer.clock.currentTime = Cesium.JulianDate.fromDate(new Date("2000-01-01 12:00:00"));
    viewer.clock.stopTime = Cesium.JulianDate.fromDate(new Date("2000-01-01 12:10:00"));
    // viewer.shadowMap.darkness = 0.6;

    return viewer;
};

/**
 * 获取3DTiles的变换矩阵。
 * @param center
 * @param params {Object}
 * @param params.height
 * @param params.scale
 * @param (params.longitude, params.latitude预留，暂未实现)
 * @returns {Cesium.Matrix4} 变换矩阵
 */
Cesium.gm.getTransMatrix = function(center, params) {

    // 缩放
    params.scale = 1;
    params.scale = params.scale || 1;
    let matrix = new Cesium.Matrix4(
        params.scale, 0, 0, center.x * (1 - params.scale),
        0, params.scale, 0, center.y * (1 - params.scale),
        0, 0, params.scale, center.z * (1 - params.scale),
        0, 0, 0, 1);

    let cartographic = Cesium.Cartographic.fromCartesian(center);
    params.height = params.height || 0;
    // params.longitude = params.longitude || cartographic.longitude;
    // params.latitude = params.latitude || cartographic.latitude;

    let offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, params.height);
    let surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
    let translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
    let heightMatrix = Cesium.Matrix4.fromTranslation(translation);
    // Cesium.gm.logMatrix(heightMatrix, "heightMatrix");

    // 合并
    matrix[12] = matrix[12] + heightMatrix[12];
    matrix[13] = matrix[13] + heightMatrix[13];
    matrix[14] = matrix[14] + heightMatrix[14];
    // Cesium.gm.logMatrix(matrix, "matrix");

    return matrix;
};

/**
 * 坐标操作。
 */
Cesium.gm.Position = {};
Cesium.gm.Position._hpRoll = new Cesium.HeadingPitchRoll(0.0, 0.0, 0.0);
Cesium.gm.Position._vector = new Cesium.HeadingPitchRoll(0.0, 0.0, 0.0);
Cesium.gm.Position._matrix = new Cesium.Matrix4(0.0, 0.0, 0.0);
Cesium.gm.Position._matrix2 = new Cesium.Matrix4(0.0, 0.0, 0.0);
/**
 *
 * @param position {Cesium.Cartesian3} 基准点
 * @param x {number|undefined} 偏移值。默认0
 * @param y {number|undefined} 偏移值。默认0
 * @param z {number|undefined} 偏移值。默认0
 * @param result {Cesium.Cartesian3|undefined} 返回值。
 */
Cesium.gm.Position.offset = function(position, hpRoll, x, y, z, result) {
    if (result === undefined) {
        result = new Cesium.Cartesian3();
    }

    Cesium.gm.Position._vector.x = x || 0;
    Cesium.gm.Position._vector.y = y || 0;
    Cesium.gm.Position._vector.z = z || 0;
    if (hpRoll) {
        Cesium.gm.Position._hpRoll.heading = hpRoll.heading;
        Cesium.gm.Position._hpRoll.pitch = hpRoll.pitch;
        Cesium.gm.Position._hpRoll.roll = hpRoll.roll;
    } else {
        Cesium.gm.Position._hpRoll.heading = 0;
        Cesium.gm.Position._hpRoll.pitch = 0;
        Cesium.gm.Position._hpRoll.roll = 0;
    }
    Cesium.Transforms.headingPitchRollToFixedFrame(position, Cesium.gm.Position._hpRoll,
        Cesium.Ellipsoid.WGS84, Cesium.Transforms.eastNorthUpToFixedFrame, Cesium.gm.Position._matrix);

    Cesium.Matrix4.multiplyByPoint(Cesium.gm.Position._matrix, Cesium.gm.Position._vector, result);

    return result;
};

/**
 * 计算平面坐标。
 * @param basePosition {Cesium.Cartesian3} 基准点
 * @param toPosition {Cesium.Cartesian3} 计算点位
 * @param result {Cesium.Cartesian3|undefined} 返回值。
 */
Cesium.gm.Position.toSurface = function(basePosition, toPosition, result) {
    if (result === undefined) {
        result = new Cesium.Cartesian3();
    }

    Cesium.gm.Position._vector.x = 0;
    Cesium.gm.Position._vector.y = 0;
    Cesium.gm.Position._vector.z = 0;
    Cesium.Transforms.headingPitchRollToFixedFrame(basePosition, Cesium.gm.Position._vector,
        Cesium.Ellipsoid.WGS84, Cesium.Transforms.eastNorthUpToFixedFrame, Cesium.gm.Position._matrix);

    Cesium.Matrix4.inverseTransformation(Cesium.gm.Position._matrix, Cesium.gm.Position._matrix2);
    Cesium.Matrix4.multiplyByPoint(Cesium.gm.Position._matrix2, toPosition, result);

    return result;
};


/**
 * 日志输出矩阵值
 * @param matrix
 * @param title
 */
Cesium.gm.logMatrix = function(matrix, title) {
    console.log(title || "");
    console.log(matrix[0] + ', ' + matrix[4] + ', ' + matrix[8] + ', ' + matrix[12]);
    console.log(matrix[1] + ', ' + matrix[5] + ', ' + matrix[9] + ', ' + matrix[13]);
    console.log(matrix[2] + ', ' + matrix[6] + ', ' + matrix[10] + ', ' + matrix[14]);
    console.log(matrix[3] + ', ' + matrix[7] + ', ' + matrix[11] + ', ' + matrix[15]);
};

/**
 * 在地图上显示坐标。
 * @param viewer {Cesium.viewer}
 * @param eventType {Cesium.ScreenSpaceEventType} 触发方式。默认：MOUSE_MOVE
 */
Cesium.gm.showMousePointCoord = function(viewer, eventType) {
    eventType = eventType || Cesium.ScreenSpaceEventType.MOUSE_MOVE;
    // eventType = eventType || Cesium.ScreenSpaceEventType.LEFT_CLICK;

    // 添加页面Element元素显示坐标文本。
    let coordinateDiv = document.createElement("div");
    coordinateDiv.style.position = 'absolute';
    coordinateDiv.style.bottom = '10px';
    coordinateDiv.style.right = '10px';
    coordinateDiv.style.color = '#A0A0A0';
    coordinateDiv.innerText = "";
    viewer.container.appendChild(coordinateDiv);

    let ellipsoid = viewer.scene.globe.ellipsoid;
    let handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function(movement) {
        //通过指定的椭球或者地图对应的坐标系，将鼠标的二维坐标转换为对应椭球体三维坐标
        let cartesian = viewer.camera.pickEllipsoid(movement.endPosition || movement.position, ellipsoid);
        if (cartesian) {
            //将笛卡尔坐标转换为地理坐标
            let cartographic = ellipsoid.cartesianToCartographic(cartesian);
            //将弧度转为度的十进制度表示
            let longitudeString = Cesium.Math.toDegrees(cartographic.longitude).toFixed(5);
            let latitudeString = Cesium.Math.toDegrees(cartographic.latitude).toFixed(5);
            //获取相机高度
            let height = Math.ceil(viewer.camera.positionCartographic.height).toFixed(3);
            coordinateDiv.innerText = 'X:' + longitudeString + ', Y:' + latitudeString + ", H:" + height;
        } else {
            coordinateDiv.innerText = "";
        }
    }, eventType);
};

Cesium.gm.viewModel = function(viewer) {

};

/**
 * 添加GeoServer发布的Wmts服务
 * @param opt.url {String} 服务地址
 * @param opt.layer {String} 服务图层名称
 * @param opt.tileMatrixSetID {String|undefined} 参考坐标系。默认EPSG:4326
 * @param opt.format {String|undefined} 默认image/png
 * @param opt.maximumLevel {Number|undefined} 默认21
 * @returns {Cesium.WebMapTileServiceImageryProvider}
 */
Cesium.gm.createGeoServerWmtsLayer = function(opt) {
    let tileMatrixSetID = opt.tileMatrixSetID || 'EPSG:4326';
    let tileMatrixLabels = [];
    for (let i = 0; i < 20; i++) {
        tileMatrixLabels.push(tileMatrixSetID + ":" + i);
    }

    return new Cesium.WebMapTileServiceImageryProvider({
        url: opt.url,
        style: '',
        format: opt.format || 'image/png',
        layer: opt.layer,
        tileMatrixSetID: tileMatrixSetID,
        tilingScheme: new Cesium.GeographicTilingScheme(),
        tileMatrixLabels: tileMatrixLabels,
        maximumLevel: opt.maximumLevel || 21,
    })
};

/**
 * 加载3DTiles模型。
 * @param opt {Object}
 * @param opt.url {String} 模型地址
 * @param opt.viewer {Cesium.viewer}
 * @param opt.transParam {Object|undefined} 模型转换参数
 * @param opt.autoLookAt {boolean|undefined} 是否设置相机位置。默认false
 * @param opt.lookAt {Object|undefined} 指定相机位置
 * @param callback {function|undefined} 加载模型完毕后的回调函数
 * @return {Cesium.Cesium3DTileset} 模型对象
 */
Cesium.gm.add3DTiles = function(opt, callback) {
    let tileset = opt.viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
        url: opt.url,
        maximumMemoryUsage: 1024,
        // shadows: Cesium.ShadowMode.DISABLED,
        // preferLeaves: true,
        // debugShowContentBoundingVolume: true,
        luminanceAtZenith: 0.4,
        // maximumScreenSpaceError: 8
    }));

    tileset.readyPromise.then(function(tileset) {
        // 调整大小和高度
        if (opt.transParam) {
            tileset.modelMatrix = Cesium.gm.getTransMatrix(tileset.boundingSphere.center, opt.transParam);
        }

        // 设置视角
        if (opt.autoLookAt || opt.lookAt) {
            let boundingSphere = tileset.boundingSphere;
            opt.lookAt = opt.lookAt || {};
            opt.lookAt.heading = opt.lookAt.heading !== undefined ? opt.lookAt.heading : 0;
            opt.lookAt.pitch = opt.lookAt.pitch !== undefined ? opt.lookAt.pitch : -Math.PI / 4.0;
            opt.lookAt.range = opt.lookAt.range !== undefined ? opt.lookAt.range : boundingSphere.radius * 1.0;
            let hpr = new Cesium.HeadingPitchRange(opt.lookAt.heading, opt.lookAt.pitch, opt.lookAt.range);

            opt.viewer.camera.viewBoundingSphere(boundingSphere, hpr);
            opt.viewer.camera.lookAtTransform(Cesium.Matrix4.IDENTITY);
        }

        if (callback) {
            callback(tileset);
        }
    }).otherwise(function(error) {
        throw (error);
    });

    return tileset;
};

/**
 * 加载Gltf模型。
 * @param opt.viewer {Cesium.viewer}
 * @param opt.location {Cesium.Cartesian3} 模型放置位置
 * @param opt.scale {number} 模型比例
 * @param opt.flyTo {boolean|undefined} 定位到。默认false
 * @param opt.height
 * @return {Cesium.Model}
 */
Cesium.gm.addGltf = function(opt, callback) {
    let modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(opt.location);
    let model = viewer.scene.primitives.add(Cesium.Model.fromGltf({
        url: 'http://192.168.180.202:8888/gm600/cams/gis/cesium-data/bds_simple/bds4.gltf',
        modelMatrix: modelMatrix,
        scale: opt.scale || 1.0 //放大倍数
    }));

    model.readyPromise.then(function(model) {
        if (opt.flyTo) {
            viewer.camera.flyTo({
                destination: opt.location
            });
        }
        if (callback) {
            callback(model);
        }
    });
};

Cesium.gm.highlightFeature = function() {
    let _this = this;
    this.features = [];
    this.originalColor = new Cesium.Color();
    this.highlightColor = Cesium.Color.YELLOW;
    // this.highlightColor = new Cesium.Color(0.8, 0.8, 0.8,1);

    this.clear = function() {
        for (let i = 0; i < _this.features.length; i++) {
            try {
                _this.features[i].color = _this.originalColor;
            } catch (err) {}
        }
        _this.features = [];
    };

    this.set = function(item) {
        if (item instanceof Array) {
            for (let j = 0; j < item.length; j++) {
                _this.features.push(item[j]);
                Cesium.Color.clone(item[j].color, _this.originalColor);
                item[j].color = _this.highlightColor;
            }
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            _this.features.push(item);
            Cesium.Color.clone(item.color, _this.originalColor);
            item.color = _this.highlightColor;
        }
    };
};

/**
 * 获取相机视角参数
 */
Cesium.gm.getCameraParam = function(camera) {
    return {
        destination: {
            x: camera.position.x.toFixed(5),
            y: camera.position.y.toFixed(5),
            z: camera.position.z.toFixed(5)
        },
        orientation: {
            heading: camera.heading.toFixed(3),
            pitch: camera.pitch.toFixed(3),
            roll: camera.roll.toFixed(3)
        }
    }
};

/**
 * 场景模型表
 * @param viewer {Cesium.viewer}
 * @param tileset {Cesium.Cesium3DTileset}
 * @param idField {String|undefined} 作为ID的属性字段默认name
 * @param filterFunc {function|undefined} 返回true表示加入，false不加入。
 */
Cesium.gm.FeatureTable = function(viewer, tileset, idField, filterFunc) {
    let _this = this;
    this.table = {};
    this.idField = idField || 'name';
    this.originalColor = Cesium.Color.WHITE;
    this.unVisibleColor = new Cesium.Color(0, 0, 0, 0);
    this.selectColor = Cesium.Color.LIME;
    this.transparentColor = new Cesium.Color(255, 255, 255, 0.0);
    this.flickerColor = Cesium.Color.RED;
    this.flickerIds = [];
    this.flickerInterval = [2000, 800];
    this.filterFunc = filterFunc;

    // 轮廓效果
    let _silhouettedGroup = [];
    let _silhouette = Cesium.PostProcessStageLibrary.createEdgeDetectionStage();
    _silhouette.uniforms.color = Cesium.Color.LIME;
    _silhouette.uniforms.length = 0.01;
    _silhouette.selected = [];
    viewer.scene.postProcessStages.add(Cesium.PostProcessStageLibrary.createSilhouetteStage([_silhouette]));

    // 监听Feature加载和卸载事件。
    new function() {
        tileset.tileLoad.addEventListener(function(tile) {
            processTileFeatures(tile, function(feature) {
                _this.load(feature);
            });
        });

        tileset.tileUnload.addEventListener(function(tile) {
            processTileFeatures(tile, function(feature) {
                _this.unload(feature);
            });
        });

        function processTileFeatures(tile, callback) {
            let content = tile.content;
            let innerContents = content.innerContents;
            if (Cesium.defined(innerContents)) {
                let length = innerContents.length;
                for (let i = 0; i < length; ++i) {
                    processContentFeatures(innerContents[i], callback);
                }
            } else {
                processContentFeatures(content, callback);
            }
        }

        function processContentFeatures(content, callback) {
            for (let i = 0, ii = content.featuresLength; i < ii; ++i) {
                let feature = content.getFeature(i);
                callback(feature);
            }
        }
    };

    // 闪烁控制
    let start = 0;
    let flag = true;

    function step(timestamp) {
        let progress = timestamp - start;
        let interval = flag ? _this.flickerInterval[0] : _this.flickerInterval[1];
        if (progress > interval) {
            for (let i = 0, ii = _this.flickerIds.length; i < ii; i++) {
                let group = _this.table[_this.flickerIds[i]];
                for (let j = 0, jj = group.features.length; j < jj; j++) {
                    if (flag) {
                        group.features[j].color = _this.originalColor;
                    } else {
                        group.features[j].color = _this.flickerColor;
                    }
                }
            }

            flag = !flag;
            start = timestamp;
        }

        window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);


    /**
     * 加载feature到表中。
     */
    this.load = function(feature) {
        let id = feature.getProperty(_this.idField);
        if (_this.filterFunc) {
            if (!_this.filterFunc(id)) {
                return;
            }
        }

        let group = _this.loadId(id);

        if (group.features.indexOf(feature) < 0) {
            group.features.push(feature);

            if (group.flicker) {
                feature.color = _this.flickerColor;
            }

            if (group.select) {
                feature.color = _this.selectColor;
            }

            if (group.silhouette) {
                _silhouette.selected.push(feature);
            }

            if (!group.visible) {
                feature.color = _this.unVisibleColor;
            }

            if (group.transparent) {
                feature.color = _this.transparentColor;
            }
        }
    };

    /**
     * 预加载ID到表中。
     * @param id {String}
     * @param flicker {boolean|undefined} 默认false
     * @param highlight {boolean|undefined} 默认false
     * @param visible {boolean|undefined} 默认true
     * @param silhouette {boolean|undefined} 默认false
     * @return Object
     */
    this.loadId = function(id, highlight, flicker, visible, silhouette) {
        if (_this.filterFunc) {
            if (!_this.filterFunc(id)) {
                return;
            }
        }

        let group = _this.table[id];
        if (!group) {
            group = {
                id: id,
                features: [],
                flicker: Cesium.defined(flicker) ? flicker : false,
                highlight: Cesium.defined(highlight) ? highlight : false,
                visible: Cesium.defined(visible) ? visible : true,
                silhouette: Cesium.defined(silhouette) ? silhouette : false
            };
            _this.table[id] = group;
        }

        return group;
    };

    /**
     * 从表中卸载feature
     */
    this.unload = function(feature) {
        let id = feature.getProperty(_this.idField);
        let group = _this.table[id];
        // if (group) {
        //     return;
        // }

        let index = group.features.indexOf(feature);
        // group.features[index].color = _this.originalColor;
        group.features.splice(index, 1);
        // _silhouette.selected.splice(index, 1);
    };

    /**
     * 闪烁对象。
     * @param item {String|Array|Cesium.Cesium3DTileFeature}
     */
    this.flicker = function(item) {
        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            if (_this.flickerIds.indexOf(ids[i]) >= 0) {
                continue;
            }

            let group = _this.loadId(ids[i]);
            group.flicker = true;

            _this.flickerIds.push(group.id);
        }
    };

    /**
     * 取消闪烁
     * @param item {String|Array|Cesium.Cesium3DTileFeature}
     */
    this.unFlicker = function(item) {
        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            let group = _this.loadId(ids[i]);
            if (group.flicker) {
                group.flicker = false;
                let index = _this.flickerIds.indexOf(group.id);
                _this.flickerIds.splice(index, 1);

                for (let i = 0, ii = group.features.length; i < ii; i++) {
                    group.features[i].color = _this.originalColor;
                }
            }
        }
    };

    /**
     * 对象透明化。
     * @param item {String|Array|Cesium.Cesium3DTileFeature}
     * @param inverse {boolean|undefined} 支持选中后再选中则取消选中。默认false
     */
    this.transparent = function(item, inverse) {
        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            let group = _this.loadId(ids[i]);
            if (!group.transparent) {
                group.transparent = true;
                for (let i = 0; i < group.features.length; i++) {
                    group.features[i].color = _this.transparentColor;
                }
            } else if (inverse) {
                _this.unTransparent(ids[i]);
            }
        }
    };

    /**
     * 实体化，反透明。
     */
    this.unTransparent = function(item) {
        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            let group = _this.table[ids[i]];
            if (group && group.transparent) {
                group.transparent = false;
                for (let i = 0, ii = group.features.length; i < ii; i++) {
                    group.features[i].color = _this.originalColor;
                }
            }
        }
    };

    /**
     * 取消所有透明。
     */
    this.unTransparentAll = function() {
        for (let id in _this.table) {
            let group = _this.table[id];
            if (group && group.transparent) {
                group.transparent = false;
                for (let i = 0, ii = group.features.length; i < ii; i++) {
                    group.features[i].color = _this.originalColor;
                }
            }
        }
    };

    /**
     * 选中对象。
     * @param item {String|Array|Cesium.Cesium3DTileFeature}
     * @param inverse {boolean|undefined} 支持选中后再选中则取消选中。默认false
     */
    this.select = function(item, inverse) {
        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            let group = _this.loadId(ids[i]);
            if (!group.select) {
                group.select = true;
                for (let i = 0; i < group.features.length; i++) {
                    group.features[i].color = _this.selectColor;
                }
            } else if (inverse) {
                _this.unselect(ids[i]);
            }
        }
    };

    /**
     * 取消选中模型
     */
    this.unselect = function(id) {
        let group = _this.table[id];
        if (group.select) {
            group.select = false;
            for (let i = 0, ii = group.features.length; i < ii; i++) {
                group.features[i].color = _this.originalColor;
            }
        }
    };

    this.unselectAll = function() {
        for (let id in _this.table) {
            if (_this.table[id].select) {
                _this.unselect(id);
            }
        }
    };

    /**
     * 是否已选中。
     */
    this.isSelect = function(feature) {
        let id = feature.getProperty(_this.idField);
        let group = _this.table[id];
        return group.select;
    };

    this.getSelectedFeatures = function() {
        let selectedFeatures = [];
        for (let id in _this.table) {
            if (_this.table[id].select) {
                selectedFeatures = selectedFeatures.concat(_this.table[id].features);
            }
        }
        return selectedFeatures;
    };

    /**
     * 隐藏对象。
     * @param autoUnselect: 如果之前已选中视为不隐藏。
     */
    this.unVisible = function(id) {
        let group = _this.table[id];
        if (group.visible) {
            group.visible = false;

            for (let i = 0; i < group.features.length; i++) {
                group.features[i].color = _this.unVisibleColor;
            }
        }
    };

    this.visible = function(id) {
        let group = _this.table[id];
        if (!group.visible) {
            group.visible = true;

            for (let i = 0, ii = group.features.length; i < ii; i++) {
                group.features[i].color = _this.originalColor;
            }
        }
    };

    /**
     * 选中模型的轮廓效果
     */
    this.silhouette = function(item, singleSelect) {
        if (singleSelect) {
            _this.clearSilhouette();
        }

        let ids = [];
        if (item instanceof Array) {
            ids = item;
        } else if (item instanceof Cesium.Cesium3DTileFeature) {
            let id = item.getProperty(_this.idField);
            ids.push(id);
        } else if (item) {
            ids.push(item);
        }

        for (let i = 0, ii = ids.length; i < ii; i++) {
            let group = _this.table[ids[i]];
            if (!group.silhouette) {
                group.silhouette = true;
                _silhouettedGroup.push(group);

                for (let i = 0, ii = group.features.length; i < ii; i++) {
                    _silhouette.selected.push(group.features[i]);
                }
            }
        }
    };

    /**
     * 取消选中模型的轮廓效果
     */
    this.unSilhouette = function(id) {
        let group = _this.table[id];
        if (group.silhouette) {
            group.silhouette = false;
            let groupIndex = _silhouettedGroup.indexOf(group);
            _silhouettedGroup.splice(groupIndex, 1);

            for (let i = _silhouette.selected.length - 1; i >= 0; i--) {
                for (let j = 0, jj = group.features.length; j < jj; j++) {
                    let index = group.features.indexOf(feature);
                    _silhouette.selected.splice(index, i);
                }
            }
        }
    };

    /**
     * 清除选中的模型轮廓效果。
     */
    this.clearSilhouette = function() {
        if (_silhouette.selected.length > 0) {
            _silhouette.selected = [];
        }

        if (_silhouettedGroup.length > 0) {
            for (let i = 0, ii = _silhouettedGroup.length; i < ii; i++) {
                _silhouettedGroup[i].silhouette = false;
            }
            _silhouettedGroup = [];
        }
    };


};

Cesium.gm.query = {};
/**
 * wfs查询
 * @param url：wfs基础请求地址
 * @param serverLayerName：图层名称
 * @param filter：请求参数，数组形式，支持PropertyIsEqualTo、PropertyIsLike。like支持"*"、"."和"！"
 *        例：[{propertyName: 'id', literal: 'abcd', compare: 'PropertyIsEqualTo'}]
 * @param callback
 */
Cesium.gm.query.wfs = function(url, serverLayerName, filter, callback) {
    url = url + '?service=WFS&request=GetFeature&version=1.0.0' +
        '&typeName=' + serverLayerName + '&maxFeatures=2000&outputFormat=json';

    let filterXml = null;
    if (filter && filter.length > 0) {
        filterXml = "";
        if (filter.length === 1) {
            filterXml = "" +
                "<Filter>" +
                "<" + filter[0].compare + ">" +
                "<PropertyName>" + filter[0].propertyName + "</PropertyName>" +
                "<Literal>" + filter[0].literal + "</Literal>" +
                "</" + filter[0].compare + ">" +
                "</Filter>";
        } else {
            for (let i = 0; i < filter.length; i++) {
                filterXml +=
                    +"<" + filter[i].compare + ">" +
                    "<PropertyName>" + filter[i].propertyName + "</PropertyName>" +
                    "<Literal>" + filter[0].literal + "</Literal>" +
                    "</" + filter[i].compare + ">";
            }

            filterXml = "<Filter><And>" + filterXml + "</And></Filter>";
        }

        url += "&FILTER=(" + filterXml + ")";
    }

    Cesium.gm.query.request(url, undefined, callback);
};

Cesium.gm.query.request = function(url, data, callback) {
    if (self.fetch) {
        fetch(url, {
            method: 'POST',
            body: data,
            mode: 'cors',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'text/plain;charset=UTF-8',
            }
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            callback(data);
        });
    } else {
        $.ajax({
            url: url,
            type: "POST",
            contentType: 'text/plain;charset=UTF-8',
            traditional: true,
            data: data,
            success: function(data) {
                callback(data);
            }
        });
    }
};

/**
 * tip
 */
Cesium.gm.NameOverlay = function(viewer) {
    let element = document.createElement('div');
    viewer.container.appendChild(element);
    element.className = 'nameOverlay';

    this.getElement = function() {
        return element;
    };

    this.show = function(top, left, textContent) {
        showOverlay(top, left, textContent);
    };

    this.showHtml = function(top, left, innerHtml) {
        showOverlay(top, left, null, innerHtml);
    };

    function showOverlay(top, left, textContent, innerHtml) {
        if (!top || !left || !(textContent || innerHtml)) {
            element.style.display = 'none';
            return;
        }
        element.style.display = 'block';
        element.style.bottom = viewer.canvas.clientHeight - top + 10 + 'px';
        element.style.left = left - 24 + 'px';
        if (textContent) {
            element.textContent = textContent;
        } else if (innerHtml) {
            element.innerHTML = innerHtml;
        }
    }

    this.hide = function() {
        element.style.display = 'none';
    }
};


/**
 * 增加阴影特效
 */
Cesium.gm.updatePostProcess = function() {
    let ambientOcclusion = viewer.scene.postProcessStages.ambientOcclusion;
    ambientOcclusion.enabled = true;
    // ambientOcclusion.uniforms.ambientOcclusionOnly = Boolean(viewModel.ambientOcclusionOnly);
    ambientOcclusion.uniforms.intensity = 3.0;
    ambientOcclusion.uniforms.bias = 0.1;
    ambientOcclusion.uniforms.lengthCap = 0.03;
    ambientOcclusion.uniforms.stepSize = 1.0;
    ambientOcclusion.uniforms.blurStepSize = 0.86;
};

Cesium.gm.Tools = {
    netPing: function(url, timeout) {
        if (!url) {
            return false;
        }
        if (!timeout) {
            timeout = 1000;
        }
        let canConnect = false;
        $.ajax({
            type: "GET",
            async: false,
            cache: false,
            timeout: timeout,
            url: url,
            success: function() {
                canConnect = true;
            },
            error: function() {
                canConnect = false;
            }
        });
        return canConnect;
    },
    isJson: function(obj) {
        if (obj) {
            let objStr = obj.toString().trim();
            if (objStr.length > 0 &&
                objStr[0] === '{' &&
                objStr[objStr.length - 1] === '}') {
                return true;
            }
            if (obj.toString() === '[object Object]') {
                return true;
            }
        }
        return false;
    },
    toJson: function(str) {
        let json = (new Function("return " + str))();
        return json;
    },
    copyJson: function(obj) {
        return $.extend({}, obj);
    }
};

/**
 * 增加鼠标滚轮进模型。
 * @param viewer
 */
Cesium.gm.wheelIntoModel = function(viewer) {
    let lastWhellCameraPosition = undefined;
    let lastWhellCameraPositionTimes = 0;
    let currentCameraPosition = viewer.camera.position;
    let ellipsoid = viewer.scene.globe.ellipsoid;
    viewer.screenSpaceEventHandler.setInputAction(function onMouseWheel(e) {
        if (e > 0 && lastWhellCameraPosition &&
            Math.abs(currentCameraPosition.x - lastWhellCameraPosition.x) < 0.001 &&
            Math.abs(currentCameraPosition.y - lastWhellCameraPosition.y) < 0.001 &&
            Math.abs(currentCameraPosition.z - lastWhellCameraPosition.z) < 0.001) {
            if (lastWhellCameraPositionTimes > 1) {
                let cameraHeight = ellipsoid.cartesianToCartographic(currentCameraPosition).height;
                viewer.camera.moveForward(cameraHeight / 100.0);
            } else {
                lastWhellCameraPositionTimes++;
            }
        } else {
            lastWhellCameraPositionTimes = 0;
        }
        lastWhellCameraPosition = currentCameraPosition.clone();
    }, Cesium.ScreenSpaceEventType.WHEEL);
};

/**
 * 增加鼠标滚轮进模型。
 * @param viewer
 */
Cesium.gm.addKeyNavigate = function(viewer) {
    let ellipsoid = viewer.scene.globe.ellipsoid;
    let camera = viewer.camera;
    let canvas = viewer.canvas;
    let flags = {
        looking: false,
        moveForward: false,
        moveBackward: false,
        moveUp: false,
        moveDown: false,
        moveLeft: false,
        moveRight: false
    };
    let keyCodeMap = {
        // ←
        37: 'moveLeft',
        // ↑
        38: 'moveForward',
        // →
        39: 'moveRight',
        // ↓
        40: 'moveBackward',
        // W
        87: 'moveForward',
        // S
        83: 'moveBackward',
        // Q
        81: 'moveDown',
        // E
        69: 'moveUp',
        // D
        68: 'moveRight',
        // A
        65: 'moveLeft',
    };

    canvas.setAttribute('tabindex', '0');
    canvas.onclick = function() {
        canvas.focus();
    };

    viewer.container.addEventListener('keydown', function(e) {
        let flagName = keyCodeMap[e.keyCode];
        if (flagName) {
            flags[flagName] = true;
        }
    }, false);

    viewer.container.addEventListener('keyup', function(e) {
        let flagName = keyCodeMap[e.keyCode];
        if (flagName) {
            flags[flagName] = false;
        }

        // test
        if (e.keyCode == 187) {
            viewer.scene.sunColor.x += 0.1;
            viewer.scene.sunColor.y += 0.1;
            viewer.scene.sunColor.z += 0.1;
        } else if (e.keyCode == 189) {
            viewer.scene.sunColor.x -= 0.1;
            viewer.scene.sunColor.y -= 0.1;
            viewer.scene.sunColor.z -= 0.1;
        } else if (e.keyCode == 48) {
            viewer.scene.sunColor.x = 0;
            viewer.scene.sunColor.y = 0;
            viewer.scene.sunColor.z = 0;
        } else if (e.keyCode == 57) {
            viewer.scene.sunColor.x = 100;
            viewer.scene.sunColor.y = 100;
            viewer.scene.sunColor.z = 100;
        }
    }, false);

    viewer.clock.onTick.addEventListener(function(clock) {
        if (!flags.moveForward && !flags.moveBackward &&
            !flags.moveUp && !flags.moveDown &&
            !flags.moveLeft && !flags.moveRight) {
            return;
        }

        let cameraHeight = ellipsoid.cartesianToCartographic(camera.position).height;
        let moveRate = cameraHeight / 100.0;

        if (flags.moveForward) {
            camera.moveForward(moveRate);
        }
        if (flags.moveBackward) {
            camera.moveBackward(moveRate);
        }
        if (flags.moveUp) {
            camera.moveUp(moveRate);
        }
        if (flags.moveDown) {
            camera.moveDown(moveRate);
        }
        if (flags.moveLeft) {
            camera.moveLeft(moveRate);
        }
        if (flags.moveRight) {
            camera.moveRight(moveRate);
        }
    });
};