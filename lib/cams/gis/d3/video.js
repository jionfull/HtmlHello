/**
 * GIS地图的视频设置。
 * 调用底层videoControl.js封装的方法。
 */
video = {};
video.playerInited = false;
/**
 * 主/子码流。默认0主码流。
 */
video.defaultStreamMode = 0;
video.init = function(params) {
    let divId = params.divId;
    let offset = params.offset || {};
    params.layout = params.layout || '1x1';
    params.playMode = params.playMode || 0;
    offset.left = offset.left || 0;
    offset.top = offset.top || 0;
    offset.right = offset.right || 0;
    offset.width = offset.width || 0;
    offset.height = offset.height || 0;

    let position = video.calcPosition(divId, offset);
    initPlugin(divId, position.left, position.top, position.width, position.height,
        params.playMode, params.layout,
        function(e) {
            video.playerInited = true;
            if (params.callback) {
                params.callback();
            }
        }, params.showToolbar, params.showSmart, params.buttonIDs
    );

    // 窗口有变化重新计算。
    window.onresize = function() {
        var playerWnd = getPlayerWndSetting(window);
        if (!playerWnd.playerWndSetting.state) {
            return;
        }
        if (!video.playerInited) {
            return;
        }
        position = video.calcPosition(divId, offset);
        rePosition(position.left, position.top, position.width, position.height);
        // 目前只改变位置。
        // oWebControl.JS_Resize(position.width,position.height);
    };

    function getPlayerWndSetting(topParent) {
        if (topParent && topParent.playerWndSetting && topParent.playerWndSetting.state != undefined) {
            return topParent;
        }
        return getPlayerWndSetting(topParent.parent);
    }
};

/**
 * 计算控件默认放置的位置
 * @param divId
 * @returns {{top: *, left: *, width: (*|number), height: (*|number)}}
 */
video.calcPosition = function(divId, offset) {
    let playerDom = document.getElementById(divId);
    let shirnkage = null;
    if (parent && parent != window) {
        if (window.parent.document.getElementById("icon_shrinkage")) {
            shirnkage = window.parent.document.getElementById("icon_shrinkage").className;
        } else if (parent != parent.parent && window.parent.parent.document.getElementById("icon_shrinkage")) {
            shirnkage = window.parent.parent.document.getElementById("icon_shrinkage").className;
        }
    }

    let left = 0;
    let top = 0;
    let width = +playerDom.clientWidth;
    let height = +playerDom.clientHeight;

    // 计算默认位置和大小。适应iframe框。
    if (shirnkage) {
        top += 80 - 7;
        if (shirnkage == 'iconfont icon-aui-icon-left icon-aui-icon-right') {
            left += 70 + offset.right;
        } else {
            left += 180;
        }
    }

    video.defaultPosition = {
        left: left,
        top: top,
        width: width,
        height: height
    };

    video.defaultPosition = {
        left: video.defaultPosition.left + offset.left,
        top: video.defaultPosition.top + offset.top,
        width: video.defaultPosition.width + offset.width,
        height: video.defaultPosition.height + offset.height
    };

    // 计算缩小后的位置和大小。
    left = 0;
    top = 0;
    width = 200;
    height = 200;
    if (shirnkage) {
        let iframe = parent.document.getElementById('mainIframeID') ||
            parent.parent.document.getElementById('mainIframeID');
        let frameHeight = iframe.contentWindow.document.body.offsetHeight;
        let frameWidth = iframe.contentWindow.document.body.offsetWidth;

        top = frameHeight - 30 - 200 - 5;
        if (shirnkage == 'iconfont icon-aui-icon-left icon-aui-icon-right') {
            left = frameWidth + 50 - 200;
        } else {
            left = frameWidth + 150 - 200;
        }
    } else {
        let frameHeight = document.body.clientHeight;
        let frameWidth = document.body.clientWidth;
        top = frameHeight - 110 - 200;
        left = frameWidth - 35 - 200;
    }

    video.minimizePosition = {
        left: left,
        top: top,
        width: width,
        height: height
    };

    return video.defaultPosition;
};

/**
 * 显示视频。
 * @param cameraId 相机ID
 */
video.preview = function(cameraId) {
    startPreview(cameraId, video.defaultStreamMode);
};

/**
 * 将视频窗口置于左上方（默认）。
 */
video.backPlayer = function() {
    player_current_state = true;
    if (parent && parent.playerWndSetting && parent.playerWndSetting.state !== undefined) {
        parent.playerWndSetting.state = true;
    }

    if (video.defaultPosition) {
        var position = video.defaultPosition;
        rePosition(position.left, position.top, position.width, position.height);
    }
};

/**
 * 将视频窗口置于右下方（最小化）
 */
video.bottomPlayer = function() {
    player_current_state = true;
    if (parent && parent.playerWndSetting && parent.playerWndSetting.state !== undefined) {
        parent.playerWndSetting.state = false;
    }

    if (video.defaultPosition) {
        var position = video.minimizePosition;
        rePosition(position.left, position.top, position.width, position.height);
    }
};

/**
 * 加载设备关联的摄像机，并且调用关联设备的预置位。
 * 默认调用关联的第一个摄像机。
 * 默认调用关联的第一个摄像机的最小编号预置位。
 * @param equipId
 */
video.previewPresetByEquipId = function(equipId, callback) {
    // 查相机。
    $.get(ctx + "/cams/gismap/cameras/" + equipId, function(result) {
        if (!result.data || result.data.length == 0) {
            if (callback) {
                callback(false, '没有关联相机。');
            }
        }

        // 默认展示第一个相机ID
        let cameraIndexCode = result.data[0].cameraIndexCode;
        video.preview(cameraIndexCode);

        // 查预置位并调用。
        $.post(ctx + "/cams/video/preset/list", {
                equipId: result.data[0].id,
                pid: equipId
            },
            function(result2) {
                if (result2 && result2.length > 0) {
                    let index = 0;
                    for (let i = 1; i < result2.length; i++) {
                        if (+result2[index].presetPointIndex > +result2[i].presetPointIndex) {
                            index = i;
                        }
                    }

                    // 调预置位
                    $.post(ctx + '/cams/video/preset/goto', {
                        equipId: result2[index].equipId,
                        presetIndex: result2[index].presetPointIndex,
                        maId: result2[index].maId,
                    }, function(resut3) {
                        if (callback) {
                            if (resut3 && resut3.status == 0) {
                                callback(true);
                            } else {
                                callback(true, '调用预置位失败。' + resut3.message);
                            }
                        }
                    });
                } else {
                    if (callback) {
                        callback(true, '没有设备对应的预置位。');
                    }
                }
            }
        );
    });
};

/**
 * 显示设备画面
 */
video.previewByEquipId = function(equipId, callback) {
    //目前其他设备没摄像头
    $.get(ctx + "/cams/gismap/cameras/" + equipId, function(result) {
        if (result.data && result.data.length > 0) {
            // 默认点击第一个相机ID
            let cameraIndexCode = result.data[0].cameraIndexCode;
            try {
                video.preview(cameraIndexCode);
            } catch (err) {
                Msg.error('海康插件调用失败！');
            }
            console.log("new previewByEquipId");
        }
        if (callback) {
            callback(result);
        }

    });
};

/**
 * 显示摄像机当前画面
 */
video.previewByCameraEquipId = function(cameraEquipId, callback) {
    $.get(ctx + '/cams/gis2d/getEquipCameraId?equipId=' + cameraEquipId, function(result) {
        if (result.data && result.data && result.data.cameraid) {
            video.preview(result.data.cameraid);

            if (callback) {
                callback(result);
            }
        }
    });
};

/**
 * 显示摄像机当前画面
 */
video.previewByCameraName = function(cameraName, callback) {
    $.get(ctx + '/cams/gis2d/findCameraIdByName', {
        subId: subId,
        cameraName: cameraName
    }, function(result) {
        if (result.data && result.data && result.data.cameraId) {
            video.preview(result.data.cameraId);

            if (callback) {
                callback(result);
            }
        }
    });
};

/**
 * 显示摄像机当前画面，如果是双摄则显示1×2双屏，单摄显示1×1单屏。
 */
video.previewByDualCameraName = function(cameraName, subId) {
    let url = ctx + "/cams/gismap/cameraList";
    ajaxSilent(url, {
        cameraName: cameraName,
        subId: subId
    }, function(result) {
        if (result.data) {
            let length = result.data.length;
            try {
                if (length > 1) {
                    setLayout("1x2");
                    for (let i = 0; i < length; i++) {
                        startPreview(result.data[i], null, null, null, i + 1);
                    }
                } else {
                    setLayout("1x1");
                    startPreview(result.data[0]);
                }
            } catch (err) {
                console.log("hk web plugin fail");
            }
        }
    });
};

video.cacheSubZoneCamera = {};

/**
 * 显示区域相机画面
 * @param subId 所亭ID
 * @param zoneCode {String|undefined} 区域编码。默认“00”：全景机位
 */
video.previewZoneCamera = function(subId, zoneCode) {
    zoneCode = zoneCode || '00';
    if (video.cacheSubZoneCamera[subId]) {
        let cameraId = video.cacheSubZoneCamera[subId][zoneCode];
        video.previewByCameraEquipId(cameraId);
    }

    $.post(ctx + "/cams/gis3d/getD3DataPath", {
        subId: subId,
        ip: window.location.hostname,
        port: window.location.port,
        protocol: window.location.protocol
    }, function(result) {
        if (result.data && result.data.d3ModelProp) {
            let d3ModelProp = eval('(' + result.data.d3ModelProp + ')');
            if (d3ModelProp && d3ModelProp.zoneCamera) {
                video.cacheSubZoneCamera[subId] = d3ModelProp.zoneCamera;
                let cameraId = video.cacheSubZoneCamera[subId][zoneCode];
                video.previewByCameraEquipId(cameraId);
            }
        }
    });
};