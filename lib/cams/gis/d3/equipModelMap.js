/**
 * 设备和模型映射
 */
let EquipModelMap = function() {
    let _this = this;
    // 设备基础信息和管理模型的ID
    this.equips = undefined;
    // 存储模型ID与设备ID的映射
    let modelEquip = undefined;
    this.ready = false;
    this.subId = undefined;

    this.init = function(subId, opt, callback) {
        _this.ready = false;
        _this.subId = subId;
        _this.equips = {};
        modelEquip = {};
        opt = opt || {};

        if (!opt.subId) {
            opt.subId = subId;
        }

        $.get(ctx + '/cams/gis3d/findEquipModelList', opt, function(result) {
            if (!result || result.status !== 0) {
                return;
            }

            for (let i = 0; i < result.data.length; i++) {
                if (!result.data[i].d3ModelId) {
                    continue;
                }

                let equip = result.data[i];
                equip.d3ModelIds = [];

                let d3ModelIds = result.data[i].d3ModelId.split(',');
                for (let j = 0; j < d3ModelIds.length; j++) {
                    if (d3ModelIds[j]) {
                        equip.d3ModelIds.push(d3ModelIds[j]);
                        modelEquip[d3ModelIds[j]] = result.data[i].id;
                    }
                }

                _this.equips[equip.id] = equip;
            }

            _this.ready = true;

            if (callback) {
                callback(result.data);
            }
        }).error(function(e) {
            var a = e;
        });
    };

    this.getModelIdsByEquipId = function(equipId) {
        let equip = _this.equips[equipId]
        return equip ? equip.d3ModelIds : null;
    };

    this.getModelIdsByModelId = function(modelId) {
        let equipId = modelEquip[modelId];
        return equipId ? _this.getModelIdsByEquipId(equipId) : null;
    };

    this.getEquipByEquipId = function(equipId) {
        return _this.equips[equipId];
    };

    this.getEquipByModelId = function(modelId) {
        let equipId = modelEquip[modelId];
        return equipId ? _this.equips[equipId] : null;
    };

    this.getEquipIdByModelId = function(modelId) {
        let equip = _this.getEquipByModelId(modelId);
        return equip ? equip.id : null;
    };


};