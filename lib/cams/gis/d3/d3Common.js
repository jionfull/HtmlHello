/*确认全部*/
function sureAll(dg) {
    sureAllById("", dg);
}

/*确认全部*/
function sureAllById(id, dg, dg2) {
    sure(id, '是否确认全部的报警信息？', dg, dg2);
}

/*确认当前条*/
function sureOne(id, dg, dg2) {
    sure(id, '是否确认该报警？', dg, dg2);
}

/*确认实时报警  ids 需要确认的实时报警id  dg表的id */
function sure(ids, msg, dg, dg2) {
    $.messager.confirm('提示', msg, function(r) {
        if (r) {
            $.ajax({
                url: ctx + '/cams/realTimeAlarm/sureAlarm',
                data: {
                    ids: ids
                },
                cache: false,
                success: function(result) {
                    if (result.status == 0) {
                        Msg.success("提示", result.message);
                        $('#ui_confirm_alarm_details').dialog('destroy');
                        window.setTimeout(function() {
                            if (checkVal(dg)) {
                                $("#" + dg).datagrid('unselectAll');
                                $("#" + dg).datagrid('reload');
                            }
                            if (checkVal(dg2)) {
                                $("#" + dg2).datagrid('unselectAll');
                                $("#" + dg2).datagrid('reload');
                            }
                        }, 100);
                    } else {
                        Msg.error("错误", result.message);
                    }
                },
                error: function() {
                    Msg.error("错误", "参数配置获取失败!");
                }
            });
        }
    });
}

//检查判断
function checkVal(obj) {
    if (typeof obj === 'undefined') {
        return false
    }
    if (obj == null) {
        return false
    }
    if (obj == '') {
        return false;
    }
    return true;
}

/*把参数对象转换成url格式*/
function getURLParams(data) {
    var params = '';
    Object.keys(data).forEach(function(key) {
        params += '&' + key + "=" + data[key];
    });
    return params;
}

/*格式化状态*/
function formatstatus(val, row, index) {
    return getValueByCodeAndType(row.status, DEFECT_STATUS, 'red');
}


/*格式化级别*/
function formatteralarmLevel(val, row, index) {
    return getValueByCodeAndType(row.alarmLevel, ALAM_LEVEL);
}

/*格式化设备 设备名称+数据点位*/
function formatEquipName(val, row, index) {
    var content = row.equipName;
    if (row.alarmDataType) {
        content += '.' + row.alarmDataType;
    }
    return content;
}

/*格式化报警类型*/
function formatteralarmType(val, row, index) {
    return getValueByCodeAndType(row.alarmType, CAMS_ALAM_TYPE);
}

/*格式化报警时间*/
function formatterAlarmDate(val, row, index) {
    if (val) {
        return val.substring(0, 19);
    } else {
        return '-/-';
    }
}

/*格式化缺陷*/
function formatfaultLevel(val, row, index) {
    return getValueByCodeAndType(row.faultLevel, DEFECT_QXDJ);
}

/*格式化状态*/
function formatterstatus(val, row, index) {
    return getValueByCodeAndType(row.status, ALARM_STATUS, 'red');
}

/*格式化 报警来源*/
function formattersecurityequiptype(val, row, index) {
    return getValueByCodeAndType(row.securityequiptype, cams_alarm_sourcetype);
}

function formatteralarmType(val, row, index) {
    return getValueByCodeAndType(row.alarmType, CAMS_ALAM_TYPE);
}

function formatNumber(val, row, index) {
    /* if(val){
         var prefix = val.substring(0,val.length-2);

         var suffix = val.substring(val.length-2);
         prefix = parseInt(prefix).toFixed(1);
         return prefix+""+suffix;
     }*/
    if (val) {
        var unit = val.replace(/\d+(.\d+)?/ig, "");
        var value = parseFloat(val);
        if (value && value != "NaN") {
            return value.toFixed(2) + unit;
        }
        return val;
    } else {
        return "";
    }

}