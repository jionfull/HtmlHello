Cesium.gm = Cesium.gm || {};

/**
 * 3D场景气泡框,new 出来后要记得添加进去
 * @param opt {Object}
 * @param opt.id {string|number} id <br/>
 * @param opt.element {document.element|undefined} element元素。<br/>
 * @param opt.content {string|undefined} 冒泡窗口内容html。<br/>
 * @param opt.position {Cesium.Cartesian3} 气泡框的位置<br/>
 * @param opt.viewer {Cesium.Viewer} 关联的cesium viewer
 * @param opt.offset {Array} popup框偏移像素值[X方向，Y方向]<br/>
 */

Cesium.gm.Overlay = function(opt) {
    let _this = this;
    let _canvasPositionTemp = new Cesium.Cartesian2();
    let _preRenderEventListener = undefined;

    opt = opt || {};

    /**
     * @type {string|number} id
     */
    this.id = opt.id;
    /**
     * @type {document.element|undefined} overlay的元素内容
     */
    this.element = opt.element;
    /**
     * @type {string} overlay的元素内容
     */
    this.content = opt.content;
    /**
     * @type {Cesium.Cartesian3} 放置Popup框的坐标
     */
    this.position = opt.position;
    /**
     * @type {Array} popup框偏移像素值[X方向，Y方向]
     */
    this.offset = opt.offset || [0, 0];
    /**
     * @type {Cesium.Viewer}
     */
    this.viewer = opt.viewer;
    /**
     * @type {Cesium.Cartesian2}
     */
    this.scratch = new Cesium.Cartesian2();

    /**
     * @private
     * 初始化Popup框
     */
    let init = function() {
        if (!_this.id) {
            _this.id = _this.idTemp.id;
            _this.idTemp.id++;
        }

        // 准备窗体元素Element
        if (!_this.element) {
            let element = document.createElement('div');
            element.id = _this.idTemp.pre + _this.id;
            element.style.position = 'absolute';
            $(document.body).append(element);
            _this.element = element;
        }

        if (_this.content) {
            _this.element.innerHTML = _this.content;
        }

        if (_this.viewer) {
            _this.setViewer(_this.viewer);
        }

        Cesium.gm.Overlay.items.push([_this.id, _this]);
    };

    /**
     * 设置关联的cesium viewer
     * @param viewer
     */
    this.setViewer = function(viewer) {
        _this.viewer = viewer;

        // 将元素装载到地图窗口
        if (_this.element) {
            _this.viewer.container.appendChild(_this.element);
        }

        _this.refresh();
        _preRenderEventListener = _this.viewer.scene.preRender.addEventListener(function() {
            _this.refresh();
        });
    };

    /**
     * 设置位置
     * @param position{Array}
     */
    this.setPosition = function(position) {
        _this.position = position;
        _this.refresh();
    };

    this.setContent = function(content) {
        _this.content = content;
        _this.element.innerHTML = _this.content;
    };

    /**
     * 获取关联的cesium viewer
     * @return {Cesium.Viewer}
     */
    this.getViewer = function() {
        return this.viewer;
    };

    this.getPosition = function() {
        return _this.position;
    };

    this.hide = function() {
        _this.element.style.display = "none";
    };

    this.show = function() {
        _this.element.style.display = "block";
    };

    /**
     * 关闭并销毁窗体。
     */
    this.destroy = function() {
        // 移除页面元素。
        let parent = _this.element.parentElement;
        parent.removeChild(_this.element);

        // 移除事件监听。
        _this.viewer.scene.preRender.removeEventListener(_preRenderEventListener);

        // 移除overlay集合。
        for (let i = 0; i < Cesium.gm.Overlay.items.length; i++) {
            if (Cesium.gm.Overlay.items[i][0] === _this.id) {
                Cesium.gm.Overlay.items.splice(i, 1);
                break;
            }
        }
    };

    /**
     * 更新图标在地图上的显示位置。
     */
    this.refresh = function() {
        if (_this.element.style.display === "none") {
            return;
        }

        if (!_this.getViewer()) {
            return;
        }

        if (!_this.position) {
            _this.close();
            return;
        }

        _this.viewer.scene.cartesianToCanvasCoordinates(_this.position, _canvasPositionTemp);
        if (Cesium.defined(_canvasPositionTemp)) {
            let left = Math.round(_this.offset[0] + _canvasPositionTemp.x) + 'px';
            let top = Math.round(_this.offset[1] + _canvasPositionTemp.y) + 'px';
            if (_canvasPositionTemp.x !== left || _this.element.style.top !== top) {
                _this.element.style.left = left;
                _this.element.style.top = top;
                _this.show();
            }
        }
    };

    init();
};

Cesium.gm.Overlay.prototype.idTemp = {
    id: 1,
    pre: 'co-'
};

Cesium.gm.Overlay.items = [];

Cesium.gm.Overlay.getItem = function(id) {
    for (let i = 0; i < this.items.length; i++) {
        if (this.items[i][0] === id) {
            return this.items[i][1];
        }
    }
};