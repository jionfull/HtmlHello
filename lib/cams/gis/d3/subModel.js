/**
 * 所亭模型对象。
 * 提供所亭的模型加载，feature管理。
 */
let SubModel = function(viewer) {
    let _this = this;
    this.subId = undefined;
    this.scenetree = undefined;
    this.transModelDist = 3.5;
    this.transModels = undefined;
    this.viewer = viewer;
    this.tileset = undefined;
    this.d3ModelProp = undefined;
    this.featureTable = undefined;
    this.selectFilterFeatures = [];
    this.autoHideOnClose = true;
    this.cameraMoveEndEventHandler = undefined;
    this.dataPath = undefined;

    /**
     * 加载所亭模型
     * @param opt {Object} 加载参数
     * @param opt.subId {String} 所亭ID。
     * @param opt.toDefaultView {boolean} 定位到全景视图。默认true。
     * @param opt.duration {Number} 定位到全景视图动画时间。
     * @param callback {function} 加载完成后回调函数。
     */
    this.loadModel = function(opt, callback) {
        _this.subId = opt.subId;
        _this.dataPath = null;

        $.post(ctx + "/cams/gis3d/getD3DataPath", {
            subId: _this.subId,
            ip: window.location.hostname,
            port: window.location.port,
            protocol: window.location.protocol
        }, function(result) {
            if (result.status != 0 || !result.data || !result.data.path) {
                Msg.warning("无模型数据！");
                if (callback) {
                    callback(undefined);
                }
                return;
            }

            _this.dataPath = result.data;
            let path = result.data.path;
            let prop = result.data.d3ModelProp;

            // 验证是否使用本地文件服务。
            if (result.data.localServerEnable &&
                result.data.localServerEnable.toUpperCase() === "TRUE" &&
                result.data.localServerUrl) {
                let localServerUrl = result.data.localServerUrl;
                let healthUrl = localServerUrl + 'health';
                if (Cesium.gm.Tools.netPing(healthUrl, 1000)) {
                    $.post(localServerUrl + "setProxyBaseUrl/", {
                        type: 'ftp',
                        baseUrl: path.substr(0, path.indexOf('/', 8))
                    }, function(result) {
                        path = localServerUrl + "proxy/ftp" + path.substr(path.indexOf('/', 8));
                    });
                }
            }

            // 地图属性。
            _this.d3ModelProp = null;
            if (prop) {
                _this.d3ModelProp = Cesium.gm.Tools.toJson(prop);
                // 选中过滤。
                _this.selectFilterFeatures = [];
                if (_this.d3ModelProp.unavailableModel) {
                    _this.selectFilterFeatures = _this.d3ModelProp.unavailableModel;
                }
            }

            // 验证并加载地图数据
            let url = path + "/tileset.json";
            if (!Cesium.gm.Tools.netPing(url)) {
                Msg.warning("无模型数据！");
                if (callback) {
                    callback(undefined);
                }
                return;
            }

            _this.tileset = Cesium.gm.add3DTiles({
                viewer: _this.viewer,
                url: url,
                // transParam: {
                //     height: 0.1,
                // },
            }, function(tileset) {
                // 设置Feature表
                if (tileset) {
                    _this.featureTable = new Cesium.gm.FeatureTable(_this.viewer, tileset);
                }

                fetch(path + '/scenetree.json')
                    .then(function(response) {
                        if (response.ok) {
                            return response.json();
                        } else {
                            return {};
                        }
                    }).then(function(data) {
                        _this.scenetree = {};
                        let childrenList = [];
                        childrenList.push(data.scenes[0].children);

                        while (childrenList.length > 0) {
                            let children = childrenList.pop();
                            for (let i = 0, ii = children.length; i < ii; i++) {
                                if (children[i].children) {
                                    childrenList.push(children[i].children);
                                    continue;
                                }

                                _this.scenetree[children[i].id] = children[i].sphere;
                            }
                        }

                        _this.setAutoHideOnClose(_this.autoHideOnClose);

                        // 定位到全景视图。默认“true”
                        if (opt.toDefaultView || opt.toDefaultView === undefined) {
                            _this.toView(undefined, opt.duration);
                        }

                        if (callback) {
                            callback(tileset);
                        }
                    }).catch(function(error) {});
            });

            _this.tileset.colorBlendMode = Cesium.Cesium3DTileColorBlendMode.REPLACE;
        }).error(function() {
            if (callback) {
                callback(undefined);
            }
        });
    };

    /**
     * 设置模型近处透视
     * @param autoHide
     */
    this.setAutoHideOnClose = function(autoHide) {
        let calcAndTrans = function() {
            for (let i = 0; i < _this.transModels.length; i++) {
                if (_this.viewer.camera.position.x >= _this.transModels[i].minX &&
                    _this.viewer.camera.position.y >= _this.transModels[i].minY &&
                    _this.viewer.camera.position.z >= _this.transModels[i].minZ &&
                    _this.viewer.camera.position.x <= _this.transModels[i].maxX &&
                    _this.viewer.camera.position.y <= _this.transModels[i].maxY &&
                    _this.viewer.camera.position.z <= _this.transModels[i].maxZ) {
                    _this.featureTable.transparent(_this.transModels[i].id);
                } else {
                    _this.featureTable.unTransparent(_this.transModels[i].id);
                }
            }
        };

        _this.autoHideOnClose = autoHide;
        if (!autoHide) {
            for (let i = 0; i < _this.transModels.length; i++) {
                _this.featureTable.unTransparent(_this.transModels[i].id);
            }
            return;
        }

        // 延迟初始化
        if (!_this.cameraMoveEndEventHandler) {
            _this.transModels = [];
            $.get(ctx + "/cams/gis3d/getGisDic", function(result) {
                _this.transModelDist = (result && result.status == 0 && result.data.autoTransModelDist) ?
                    +result.data.autoTransModelDist : _this.transModelDist;
                for (let i = 0; i < _this.d3ModelProp.transparentModel.length; i++) {
                    let sphere = _this.scenetree[_this.d3ModelProp.transparentModel[i]];
                    if (sphere) {
                        let item = {
                            minX: sphere[0] - sphere[3] - _this.transModelDist,
                            minY: sphere[1] - sphere[3] - _this.transModelDist,
                            minZ: sphere[2] - sphere[3] - _this.transModelDist,
                            maxX: sphere[0] + sphere[3] + _this.transModelDist,
                            maxY: sphere[1] + sphere[3] + _this.transModelDist,
                            maxZ: sphere[2] + sphere[3] + _this.transModelDist,
                            id: _this.d3ModelProp.transparentModel[i]
                        };
                        _this.transModels.push(item);
                    }
                }

                _this.cameraMoveEndEventHandler = _this.viewer.camera.changed.addEventListener(function() {
                    if (!_this.autoHideOnClose) {
                        return;
                    }
                    calcAndTrans();
                });
            });
        }

        calcAndTrans();
    };

    /**
     * 视图移动到指定区域
     * @param zoneCode {String|undefined} 区域编号。默认全景“00”
     * @param duration {number|undefined} 视图定位动画时间。
     * @param callback {function|undefined} 视图定位完毕后调用。
     */
    this.toView = function(zoneCode, duration, callback) {
        zoneCode = zoneCode || '00';

        if (!_this.d3ModelProp || !_this.d3ModelProp.viewParam || !_this.d3ModelProp.viewParam[zoneCode]) {
            let boundingSphere = _this.tileset.boundingSphere;
            let hpr = new Cesium.HeadingPitchRange(0, -Math.PI / 4.0, boundingSphere.radius * 1.0);
            _this.viewer.camera.viewBoundingSphere(boundingSphere, hpr);
            _this.viewer.camera.lookAtTransform(Cesium.Matrix4.IDENTITY);

            return;
        }

        if (duration === null || duration === undefined || duration === '') {
            duration = 2;
        }

        let viewParam = _this.d3ModelProp.viewParam[zoneCode];
        let destination = new Cesium.Cartesian3(+viewParam.destination.x, +viewParam.destination.y, +viewParam.destination.z);
        let orientation = {
            heading: +viewParam.orientation.heading,
            pitch: +viewParam.orientation.pitch,
            roll: +viewParam.orientation.roll
        };

        if (duration === 0) {
            viewer.camera.setView({
                destination: destination,
                orientation: orientation
            });
        } else {
            viewer.camera.flyTo({
                destination: destination,
                orientation: orientation,
                duration: duration,
                complete: function() {
                    if (callback) {
                        callback();
                    }
                }
            });
        }
    };

    /**
     * 默认视角。
     */
    this.toDefaultView = function() {
        _this.toView();
    };

    /**
     * 定位到设备
     * @param subModel
     * @param equipId
     */
    this.toEquipView = function(equipModelMap, equipId) {
        let equip = subModel.getEquipByEquipId(equipId);
        let d3ModelProp = Cesium.gm.Tools.isJson(equip.d3ModelProp) ?
            Cesium.gm.Tools.toJson(equip.d3ModelProp) : {};
        if (d3ModelProp.viewParam) {
            viewer.camera.flyTo({
                destination: d3ModelProp.viewParam.destination,
                orientation: d3ModelProp.viewParam.orientation,
                duration: 2
            });
        }
    };

    /**
     * 刷新报警设备。
     */
    this.refreshAlarmEquip = function(equipModelMap) {
        if (!_this.featureTable) {
            return;
        }

        $.get(ctx + '/cams/realTimeAlarm/findAlarmEquipList?subId=' + _this.subId, function(result) {
            if (result && result.status == 0) {
                let flickerIds = [];
                for (let i = 0, ii = result.data.length; i < ii; i++) {
                    let modelIds = equipModelMap.getModelIdsByEquipId(result.data[i].equipId);
                    if (modelIds) {
                        flickerIds = flickerIds.concat(modelIds);
                    }
                }

                let unFlickerIds = [];
                for (let i = 0, ii = _this.featureTable.flickerIds.length; i < ii; i++) {
                    let flickerId = _this.featureTable.flickerIds[i];
                    if (flickerIds.indexOf(flickerId) < 0) {
                        unFlickerIds.push(flickerId);
                    }
                }

                _this.featureTable.unFlicker(unFlickerIds);
                _this.featureTable.flicker(flickerIds);
            }
        })
    };

    /**
     * 设置透明
     * 根据变电所地图配置中选中的透明模型进行设置。
     */
    this.transparentModel = function(show) {
        if (!_this.d3ModelProp.transparentModel) {
            return;
        }

        if (show) {
            _this.featureTable.transparent(_this.d3ModelProp.transparentModel);
        } else {
            _this.featureTable.unTransparent(_this.d3ModelProp.transparentModel);
        }
    };

    /**
     * 不可选中
     * @return {boolean} true：不可选中。false：可选中。
     */
    this.selectFilter = function(feature) {
        if (!feature.getProperty) {
            return false;
        }

        let name = feature.getProperty('name');
        for (let i = 0; i < _this.selectFilterFeatures.length; i++) {
            if (name === _this.selectFilterFeatures[i]) {
                return true;
            }
        }

        return false;
    };


};