/**
 * 移动设备
 */
Cesium.gm.MobileModel = function() {
    let _this = this;
    let _fixedFrameTransform = Cesium.Transforms.eastNorthUpToFixedFrame;
    let _debugZeros = undefined;
    let _animationFrameRate = 24;
    let _animationDuration = 1000;
    let _animationHandler = undefined;
    let _onPositionChangedEvents = [];

    this.id = undefined;
    this.ready = undefined;
    this.viewer = undefined;
    this.opt = undefined;

    // 原始坐标位置和转置矩阵。
    this.originalHpRoll = new Cesium.HeadingPitchRoll(0.0, 0.0, 0.0);
    this.originalPosition = new Cesium.Cartesian3();
    this.originalMatrix = new Cesium.Matrix4();
    this.oraginalDebugZero = undefined;
    this.oraginalDebugAxis = undefined;

    // 基准坐标位置和转置。
    this.baseHpRoll = new Cesium.HeadingPitchRoll(0.0, 0.0, 0.0);
    this.basePosition = new Cesium.Cartesian3();
    this.baseMatrix = new Cesium.Matrix4();
    this.baseSurface = new Cesium.Cartesian3();
    this.baseInverseMatrix = new Cesium.Matrix4();
    this.baseDebugZero = undefined;
    this.baseDebugAxis = undefined;

    // 模型位置
    this.modelPrimitive = undefined;
    this.modelHpRoll = new Cesium.HeadingPitchRoll(0.0, 0.0, 0.0);
    this.modelPosition = new Cesium.Cartesian3();
    this.modelMatrix = new Cesium.Matrix4();
    this.modelSurface = new Cesium.Cartesian3();

    /**
     * 加载模型
     * @param opt
     * @param opt.id
     * @param opt.viewer
     * @param opt.modelUrl 模型地址
     * @param opt.position 加载初始位置。
     * @param opt.showDebug 显示模参考坐标轴。默认false
     * @param opt.callback 加载完成回调。
     */
    this.load = function(opt) {
        _this.ready = false;
        _this.clear();

        _this.id = opt.id;
        _this.opt = opt;
        _this.viewer = opt.viewer;

        // 计算原始点位和原始地平面转置矩阵
        _this.opt.position.clone(_this.originalPosition);
        Cesium.Transforms.headingPitchRollToFixedFrame(_this.originalPosition, _this.originalHpRoll,
            Cesium.Ellipsoid.WGS84, _fixedFrameTransform, _this.originalMatrix);

        // 基准点初始化
        _this.originalHpRoll.clone(_this.baseHpRoll);
        _this.originalPosition.clone(_this.basePosition);
        _this.originalMatrix.clone(_this.baseMatrix);
        _this.baseSurface.x = 0;
        _this.baseSurface.y = 0;
        _this.baseSurface.z = 0;
        Cesium.Matrix4.inverseTransformation(_this.baseMatrix, _this.baseInverseMatrix);

        // 模型点初始化
        _this.originalHpRoll.clone(_this.modelHpRoll);
        _this.originalPosition.clone(_this.modelPosition);
        _this.originalMatrix.clone(_this.modelMatrix);
        _this.modelSurface.x = 0;
        _this.modelSurface.y = 0;
        _this.modelSurface.z = 0;

        _this.modelPrimitive = _this.viewer.scene.primitives.add(Cesium.Model.fromGltf({
            id: _this.id,
            url: opt.modelUrl,
            modelMatrix: _this.modelMatrix
        }));

        _this.modelPrimitive.readyPromise.then(function(model) {
            let camera = _this.viewer.camera;
            let controller = _this.viewer.scene.screenSpaceCameraController;
            let radius = 2.0 * Math.max(model.boundingSphere.radius, camera.frustum.near);
            controller.minimumZoomDistance = radius * 0.5;

            if (_this.opt.callback) {
                _this.opt.callback();
            }

            _this.ready = true;
        });

        if (opt.showDebug) {
            // 原始基础调试坐标轴显示
            _this.oraginalDebugAxis = _this.viewer.scene.primitives.add(new Cesium.DebugModelMatrixPrimitive({
                modelMatrix: _this.originalMatrix,
                length: 100.0,
                width: 10.0
            }));

            // 机器人调试坐标轴显示
            _this.baseDebugAxis = _this.viewer.scene.primitives.add(new Cesium.DebugModelMatrixPrimitive({
                modelMatrix: _this.baseMatrix,
                length: 50.0,
                width: 2.0
            }));

            // 坐标定点标记。
            _debugZeros = _this.viewer.scene.primitives.add(new Cesium.PointPrimitiveCollection());
            _this.oraginalDebugZero = _debugZeros.add({
                position: _this.originalPosition,
                pixelSize: 30,
                color: Cesium.Color.YELLOW
            });
            _this.baseDebugZero = _debugZeros.add({
                position: _this.basePosition,
                pixelSize: 15,
                color: Cesium.Color.YELLOW
            });
        }
    };

    this.clear = function() {
        _this.id = undefined;

        if (_this.modelPrimitive) {
            if (_this.viewer.scene.primitives.remove(_this.modelPrimitive)) {
                _this.modelPrimitive.destroy();
            }
        }
        if (_this.oraginalDebugAxis) {
            if (_this.viewer.scene.primitives.remove(_this.oraginalDebugAxis)) {
                _this.oraginalDebugAxis.destroy();
            }
        }
        if (_this.baseDebugAxis) {
            if (_this.viewer.scene.primitives.remove(_this.baseDebugAxis)) {
                _this.baseDebugAxis.destroy();
            }
        }
        if (_this.oraginalDebugZero) {
            _debugZeros.remove(_this.oraginalDebugZero);
        }
        if (_this.baseDebugZero) {
            _debugZeros.remove(_this.baseDebugZero);
        }
        if (_debugZeros) {
            if (_this.viewer.scene.primitives.remove(_debugZeros)) {
                _debugZeros.destroy();
            }
        }
    };

    /**
     * 设备坐标框架偏移参数设置
     * @param opt
     * @param opt.offsetX
     * @param opt.offsetY
     * @param opt.offsetZ
     * @param opt.rotation
     */
    this.offset = function(opt) {
        _this.baseSurface.x = opt.offsetX || 0;
        _this.baseSurface.y = opt.offsetY || 0;
        _this.baseSurface.z = opt.offsetZ || 0;

        // 复位
        _this.originalPosition.clone(_this.basePosition);
        _this.originalHpRoll.clone(_this.baseHpRoll);
        _this.baseHpRoll.heading = Cesium.Math.toRadians(opt.rotation || 0);

        Cesium.Matrix4.multiplyByPoint(_this.originalMatrix, _this.baseSurface, _this.basePosition);

        Cesium.Transforms.headingPitchRollToFixedFrame(_this.basePosition, _this.baseHpRoll,
            Cesium.Ellipsoid.WGS84, _fixedFrameTransform, _this.baseMatrix);
        Cesium.Matrix4.inverseTransformation(_this.baseMatrix, _this.baseInverseMatrix);

        _this.baseHpRoll.clone(_this.modelHpRoll);

        if (_this.opt.showDebug) {
            _this.baseDebugZero.position = _this.basePosition;
            _this.baseDebugAxis.modelMatrix = _this.baseMatrix;
        }
    };

    /**
     * 设备坐标框架偏移参数设置
     * @param opt
     * @param opt.positionX
     * @param opt.positionY
     * @param opt.positionZ
     */
    this.toPoint = function(opt) {
        _this.modelSurface.x = opt.positionX || 0;
        _this.modelSurface.y = opt.positionY || 0;
        _this.modelSurface.z = opt.positionZ || 0;

        Cesium.Matrix4.multiplyByPoint(_this.baseMatrix, _this.modelSurface, _this.modelPosition);

        // 利用modelMatrix设置机器人的位置和方向（转换矩阵）。
        Cesium.Transforms.headingPitchRollToFixedFrame(_this.modelPosition, _this.modelHpRoll,
            Cesium.Ellipsoid.WGS84, _fixedFrameTransform, _this.modelPrimitive.modelMatrix);
        _this.modelPrimitive.modelMatrix.clone(_this.modelMatrix);

        if (_onPositionChangedEvents.length > 0) {
            for (let i = 0; i < _onPositionChangedEvents.length; i++) {
                _onPositionChangedEvents[i](_this.modelPosition);
            }
        }
    };

    /**
     * 设备坐标框架偏移参数设置
     * @param opt
     * @param opt.positionX
     * @param opt.positionY
     * @param opt.positionZ
     * @param opt.duration {number} 动画持续时间。单位毫秒，默认1000
     * @param opt.autoRotation {boolean} 自动转向。默认：true
     */
    this.toPointAnimation = function(opt) {
        _this.modelSurface.x = opt.positionX || 0;
        _this.modelSurface.y = opt.positionY || 0;
        _this.modelSurface.z = opt.positionZ || 0;

        if (_animationHandler) {
            clearInterval(_animationHandler);
            _animationHandler = null;
        }

        let beginPosition = _this.modelPosition.clone(new Cesium.Cartesian3());
        let endPosition = Cesium.Matrix4.multiplyByPoint(_this.baseMatrix, _this.modelSurface, new Cesium.Cartesian3());
        if (Math.abs(endPosition.x - beginPosition.x) < 0.001 &&
            Math.abs(endPosition.y - beginPosition.y) < 0.001 &&
            Math.abs(endPosition.z - beginPosition.z) < 0.001) {
            return;
        }

        // 方向
        if (opt.autoRotation === undefined || opt.autoRotation === true) {
            let lastSurfacePosition = Cesium.Matrix4.multiplyByPoint(_this.baseInverseMatrix, _this.modelPosition, new Cesium.Cartesian3());
            _this.modelHpRoll.heading = _this.baseHpRoll.heading -
                Math.atan2(opt.positionY - lastSurfacePosition.y, opt.positionX - lastSurfacePosition.x) || 0;
        }

        let index = 1;
        let duration = opt.duration || _animationDuration;
        let frameCount = duration / 1000 * _animationFrameRate;
        let intervalTime = 1000 / _animationFrameRate;

        // 动画过程。
        if (!opt.highlight) {
            _this.modelPrimitive.color = Cesium.Color.LIME;
        }
        _animationHandler = setInterval(function() {
            _this.modelPosition.x = beginPosition.x + (endPosition.x - beginPosition.x) / frameCount * index;
            _this.modelPosition.y = beginPosition.y + (endPosition.y - beginPosition.y) / frameCount * index;
            _this.modelPosition.z = beginPosition.z + (endPosition.z - beginPosition.z) / frameCount * index;

            Cesium.Transforms.headingPitchRollToFixedFrame(_this.modelPosition, _this.modelHpRoll,
                Cesium.Ellipsoid.WGS84, _fixedFrameTransform, _this.modelPrimitive.modelMatrix);

            if (_onPositionChangedEvents.length > 0) {
                for (let i = 0; i < _onPositionChangedEvents.length; i++) {
                    opt.index = index;
                    opt.position = _this.modelPosition;
                    _onPositionChangedEvents[i](opt);
                }
            }

            index++;
            if (index > frameCount) {
                clearInterval(_animationHandler);
                _animationHandler = null;

                if (!opt.highlight) {
                    _this.modelPrimitive.color = Cesium.Color.WHITE;
                }
            }
        }, intervalTime);

    };

    this.onPositionChanged = function(callback) {
        _onPositionChangedEvents.push(callback);
    }
};

/**
 * 空轨设备，组合相机和相机伸缩杆。
 */
Cesium.gm.Kg = function() {
    let _this = this;
    this.id = undefined;
    this.ready = undefined;
    this.xj = new Cesium.gm.MobileModel();
    this.xjg = new Cesium.gm.MobileModel();
    this.baseInverseMatrix = undefined;
    this.basePosition = undefined;
    this.baseHpRoll = undefined;
    this.basePosition = undefined;

    let _animationFrameRate = 24;
    let _animationDuration = 1000;
    let _animationHandler = undefined;
    let _onPositionChangedEvents = [];
    let _fixedFrameTransform = Cesium.Transforms.eastNorthUpToFixedFrame;

    /**
     * 加载模型
     * @param opt
     * @param opt.id
     * @param opt.viewer
     * @param opt.xjUrl 模型地址
     * @param opt.xjgUrl 模型地址
     * @param opt.position 加载初始位置。
     * @param opt.showDebug 显示模参考坐标轴。默认false
     * @param opt.callback 加载完成回调。
     */
    this.load = function(opt) {
        _this.ready = false;
        _this.id = opt.id;

        let jxOpt = Cesium.gm.Tools.copyJson(opt);
        let jxgOpt = Cesium.gm.Tools.copyJson(opt);

        jxOpt.modelUrl = jxOpt.xjUrl;
        jxgOpt.modelUrl = jxgOpt.xjgUrl;
        jxgOpt.showDebug = false;

        let loaded = 0;
        jxOpt.callback = function() {
            loaded++;
            if (loaded === 2) {
                opt.callback();
            }
        };
        jxgOpt.callback = function() {
            loaded++;
            if (loaded === 2) {
                opt.callback();
            }
        };
        _this.xj.load(jxOpt);
        _this.xjg.load(jxgOpt);

        _this.ready = true;
    };

    this.clear = function() {
        _this.id = undefined;
        _this.xj.clear();
        _this.xjg.clear();
    };

    /**
     * 空轨坐标框架偏移参数设置
     * @param opt
     * @param opt.offsetX
     * @param opt.offsetY
     * @param opt.offsetZ
     * @param opt.rotation
     */
    this.offset = function(opt) {
        _this.xj.offset(opt);
        _this.xjg.offset(opt);
    };

    /**
     * 设备坐标框架偏移参数设置
     * @param opt
     * @param opt.positionX
     * @param opt.positionY
     * @param opt.positionZ
     */
    this.toPoint = function(opt) {
        _this.xj.toPoint(opt);
        _this.xjg.toPoint(opt);

        let heightMatrix2 = new Cesium.Matrix4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, _this.xj.modelSurface.z || 0.001, 0,
            0, 0, 0, 1);

        Cesium.Matrix4.multiply(_this.xjg.modelMatrix, heightMatrix2, _this.xjg.modelPrimitive.modelMatrix);

        _this.baseInverseMatrix = _this.xj.baseInverseMatrix;
        _this.basePosition = _this.xj.basePosition;
        _this.baseHpRoll = _this.xj.baseHpRoll;
        _this.modelPosition = _this.xj.modelPosition;
    };

    /**
     * 设备坐标框架偏移参数设置
     * @param opt
     * @param opt.positionX
     * @param opt.positionY
     * @param opt.positionZ
     * @param opt.duration {number} 动画持续时间。单位毫秒，默认1000
     * @param opt.autoRotation {boolean} 自动转向。默认：true
     * @param opt.highlight {boolean} 运动高亮。默认：false
     */
    this.toPointAnimation = function(opt) {
        if (_animationHandler) {
            clearInterval(_animationHandler);
            _animationHandler = null;
        }

        let beginLength = _this.xj.modelSurface.z;
        let endLength = opt.positionZ;

        _this.xj.modelSurface.x = opt.positionX || 0;
        _this.xj.modelSurface.y = opt.positionY || 0;
        _this.xj.modelSurface.z = opt.positionZ || 0;

        _this.xjg.modelSurface.x = opt.positionX || 0;
        _this.xjg.modelSurface.y = opt.positionY || 0;
        _this.xjg.modelSurface.z = opt.positionZ || 0;

        let beginPosition = _this.xj.modelPosition.clone(new Cesium.Cartesian3());
        let endPosition = Cesium.Matrix4.multiplyByPoint(_this.xj.baseMatrix, _this.xj.modelSurface, new Cesium.Cartesian3());
        if (Math.abs(endPosition.x - beginPosition.x) < 0.001 &&
            Math.abs(endPosition.y - beginPosition.y) < 0.001 &&
            Math.abs(endPosition.z - beginPosition.z) < 0.001) {
            return;
        }

        let index = 1;
        let duration = opt.duration || _animationDuration;
        let frameCount = duration / 1000 * _animationFrameRate;
        let intervalTime = 1000 / _animationFrameRate;

        // 动画过程。
        if (!opt.highlight) {
            _this.xj.modelPrimitive.color = Cesium.Color.LIME;
            _this.xjg.modelPrimitive.color = Cesium.Color.LIME;
        }
        _animationHandler = setInterval(function() {
            // 相机位移。
            _this.xj.modelPosition.x = beginPosition.x + (endPosition.x - beginPosition.x) / frameCount * index;
            _this.xj.modelPosition.y = beginPosition.y + (endPosition.y - beginPosition.y) / frameCount * index;
            _this.xj.modelPosition.z = beginPosition.z + (endPosition.z - beginPosition.z) / frameCount * index;

            Cesium.Transforms.headingPitchRollToFixedFrame(_this.xj.modelPosition, _this.xj.modelHpRoll,
                Cesium.Ellipsoid.WGS84, _fixedFrameTransform, _this.xj.modelPrimitive.modelMatrix);

            // 相机伸缩杆位移
            _this.xj.modelPrimitive.modelMatrix.clone(_this.xjg.modelMatrix);

            let xjgHeight = beginLength + (endLength - beginLength) / frameCount * index || 0.001;
            let heightMatrix = new Cesium.Matrix4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, xjgHeight, 0,
                0, 0, 0, 1
            );
            Cesium.Matrix4.multiply(_this.xjg.modelMatrix, heightMatrix, _this.xjg.modelPrimitive.modelMatrix);

            _this.modelPosition = _this.xj.modelPosition;

            // 触发事件。
            if (_onPositionChangedEvents.length > 0) {
                for (let i = 0; i < _onPositionChangedEvents.length; i++) {
                    opt.index = index;
                    opt.position = _this.xj.modelPosition;
                    _onPositionChangedEvents[i](opt);
                }
            }

            index++;
            if (index > frameCount) {
                clearInterval(_animationHandler);
                _animationHandler = null;

                if (!opt.highlight) {
                    _this.xj.modelPrimitive.color = Cesium.Color.WHITE;
                    _this.xjg.modelPrimitive.color = Cesium.Color.WHITE;
                }
            }
        }, intervalTime);

    };

    this.onPositionChanged = function(callback) {
        _onPositionChangedEvents.push(callback);
    }
};