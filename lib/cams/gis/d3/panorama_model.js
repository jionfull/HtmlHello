/**
 * 所亭场景移动巡检设备控制。
 */
let SubMobileModel = function() {
    let _this = this;
    this.mobileEquips = {};
    this.opt = undefined;
    this.autoRefreshInterval = 3000;

    this.getEquip = function(id) {
        if (!_this.mobileEquips) {
            return null;
        }

        return _this.mobileEquips[id];
    };

    /**
     * @param opt
     * @param opt.subId
     * @param opt.viewer
     * @param opt.position
     * @param opt.autoRefresh 自动刷新位置。默认true
     * @param opt.mobileEquip 移动设备的定义。
     * @param opt.defaultModelUrl 移动设备的定义。
     * @param opt.showOverlay 是否显示标签。默认false
     */
    this.init = function(opt) {
        _this.opt = opt;

        if (!opt.mobileEquip || opt.mobileEquip.length === 0) {
            return;
        }

        let mobileEquipData = opt.mobileEquip;
        for (let i = 0; i < mobileEquipData.length; i++) {
            if (mobileEquipData[i].enable === false ||
                mobileEquipData[i].enable === 'false') {
                continue;
            }

            let mobileEquip = Cesium.gm.Tools.copyJson(mobileEquipData[i]);

            mobileEquip.mobileModel = undefined;
            let modelUrl = undefined;
            let xjUrl = undefined;
            let xjgUrl = undefined;

            if (mobileEquip.type === 'kg') {
                mobileEquip.mobileModel = new Cesium.gm.Kg();
                xjUrl = opt.defaultModelUrl.defaultKgUrl;
                xjgUrl = opt.defaultModelUrl.defaultKggUrl;
            } else {
                mobileEquip.mobileModel = new Cesium.gm.MobileModel();
                modelUrl = opt.defaultModelUrl.defaultJqrUrl;
            }

            mobileEquip.mobileModel.load({
                id: mobileEquip.id,
                viewer: opt.viewer,
                modelUrl: modelUrl,
                xjUrl: xjUrl,
                xjgUrl: xjgUrl,
                position: opt.position,
                callback: function() {
                    // 设置位置。
                    mobileEquip.mobileModel.offset({
                        offsetX: mobileEquip.offsetX,
                        offsetY: mobileEquip.offsetY,
                        offsetZ: mobileEquip.offsetZ,
                        rotation: mobileEquip.rotation
                    });
                    mobileEquip.mobileModel.toPoint({
                        positionX: 0,
                        positionY: 0,
                        positionZ: 0
                    });

                    // 显示标签。
                    if (opt.showOverlay) {
                        let idVal = "co-" + mobileEquip.id;
                        let equipName = mobileEquip.name;
                        let classEle = (mobileEquip.type === 'kg') ? "iconfont icon-konggui" :
                            ((mobileEquip.type === 'jqr') ? "iconfont icon-jiqiren" : "iconfont icon-hongwaixiangji");
                        let offset = (mobileEquip.type === 'kg') ? [-18, -55] : [-18, -80];
                        let content = '<a  onclick="_cameraController.changeCamera(\'' + mobileEquip.id + '\', \'' + mobileEquip.name + '\')"';
                        content += " class='gis_coordin gis_ic_camera' style=''><i class='" + classEle + "'></i></a>";
                        let overlay = new Cesium.gm.Overlay({
                            id: mobileEquip.id,
                            content: content,
                            position: mobileEquip.mobileModel.modelPosition,
                            viewer: _this.opt.viewer,
                            offset: offset
                        });

                        $("#" + idVal).find("i").css({
                            "background-color": "#007dc4"
                        });
                        $("#" + idVal).attr("title", equipName);

                        $("#" + idVal).on("click", function() {
                            if (idVal2 != idVal) {
                                $("#" + idVal2).find("i").css({
                                    "background-color": "#007dc4"
                                });
                            }
                            $(this).find("i").css({
                                "background-color": "#f87e33"
                            });
                            idVal2 = idVal;
                        });

                        mobileEquip.mobileModel.onPositionChanged(function(opt) {
                            if (opt.index % 4 === 0) {
                                overlay.setPosition(opt.position);
                            }
                        });
                    }
                }
            });

            _this.mobileEquips[mobileEquip.id] = mobileEquip;
        }

        // 自动更新位置。
        if (mobileEquipData.length > 0 && opt.autoRefresh) {
            setInterval(function() {
                _this.refreshEquipPosition();
            }, _this.autoRefreshInterval);
        }
    };

    this.refreshEquipPosition = function() {
        for (let id in _this.mobileEquips) {
            $.get(ctx + '/cams/rail/getEquipPosition', {
                equipId: id,
                withCache: true,
                test: true,
                type: _this.mobileEquips[id].type
            }, function(result) {
                if (result.status == 0) {
                    _this.mobileEquips[id].mobileModel.toPointAnimation({
                        positionX: result.data.x,
                        positionY: result.data.y,
                        positionZ: result.data.z
                    });
                } else {}
            })
        }
    }
};