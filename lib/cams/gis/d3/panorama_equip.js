let Map = function() {
    let _this = this;
    this.viewer = undefined;
    this.nameOverlay = undefined;
    this.subModel = undefined;
    this.equipModelMap = undefined;
    this.target = 'cesiumContainer';
    this.subMobileModel = undefined;
    let _equipClickEvents = [];
    let _equipHoverEvents = [];
    let _lastHoverEquipId = undefined;

    /**
     * 初始化地图。
     */
    this.init = function() {
        _this.viewer = Cesium.gm.createDefaultMap({
            target: _this.target,
            imgUrl: camsGisConfigInfo.gisSzgdUrlTile,
            camsGisConfigInfo: camsGisConfigInfo
        });

        _this.nameOverlay = new Cesium.gm.NameOverlay(_this.viewer);
        _this.subModel = new SubModel(_this.viewer);
        _this.equipModelMap = new EquipModelMap();
        addClickEvent();
    };

    /**
     * 应用三维地图。
     */
    this.loadMap = function(subId, callback) {
        if (_this.tileset) {
            _this.subModel.viewer.scene.primitives.remove(_this.tileset);
            _this.tileset = undefined;
        }

        _this.subModel.loadModel({
            subId: subId,
            duration: 0
        }, function(tileset) {
            _this.available = !!tileset;
            _this.tileset = tileset;

            if (!tileset) {
                Msg.warning("无模型数据！");
            }

            _this.subMobileModel = new SubMobileModel();
            _this.subMobileModel.init({
                subId: subId,
                viewer: _this.viewer,
                position: _this.subModel.tileset.boundingSphere.center,
                mobileEquip: _this.subModel.d3ModelProp.mobileEquip,
                defaultModelUrl: _this.subModel.dataPath,
                autoRefresh: true,
                showOverlay: false
            });

            if (callback) {
                callback(tileset);
            }
        });

        _this.equipModelMap.init(subId, undefined, function() {
            setInterval(function() {
                _this.subModel.refreshAlarmEquip(_this.equipModelMap)
            }, 3000);
        });
    };

    this.destroy = function() {
        if (_this.tileset) {
            _this.subModel.viewer.scene.primitives.remove(_this.tileset);
            _this.tileset = undefined;
        }

        _this.viewer.destroy();
        $('#' + _this.target).html('');
        _this.available = false;
    };

    this.onEquipClick = function(callback) {
        _equipClickEvents.push(callback);
    };

    this.onEquipHover = function(callback) {
        _equipHoverEvents.push(callback);
    };

    function addClickEvent() {
        //鼠标点击事件
        _this.viewer.screenSpaceEventHandler.setInputAction(function onMouseClick(movement) {
            _this.subModel.featureTable.unselectAll();
            let pickedFeature = _this.viewer.scene.pick(movement.position);
            if (!Cesium.defined(pickedFeature)) {
                $('#equipment').hide();
                return;
            }

            // 名称过滤
            if (!pickedFeature ||
                _this.subModel.selectFilter(pickedFeature)) {
                $('#equipment').hide();
                return;
            }

            let equip = null;
            if (pickedFeature instanceof Cesium.Cesium3DTileFeature) {
                let name = pickedFeature.getProperty('name');
                equip = _this.equipModelMap.getEquipByModelId(name);
                if (equip) {
                    let d3ModelIds = _this.equipModelMap.getModelIdsByEquipId(equip.id);
                    if (d3ModelIds) {
                        //_this.subModel.featureTable.silhouette(d3ModelIds);
                        // _this.subModel.featureTable.select(d3ModelIds);
                    }
                }
            } else if (pickedFeature.id) {
                equip = {
                    id: pickedFeature.id
                }
            }

            if (equip) {
                for (let i = 0, ii = _equipClickEvents.length; i < ii; i++) {
                    _equipClickEvents[i](equip);
                }
            }
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);

        // 鼠标移动事件
        _this.viewer.screenSpaceEventHandler.setInputAction(function onMouseMove(movement) {
            if (!_this.subModel.featureTable) {
                return;
            }

            _this.subModel.featureTable.clearSilhouette();
            _this.nameOverlay.hide();

            let pickedFeature = _this.viewer.scene.pick(movement.endPosition);
            if (!pickedFeature ||
                _this.subModel.selectFilter(pickedFeature)) {
                for (let i = 0, ii = _equipHoverEvents.length; i < ii; i++) {
                    _equipHoverEvents[i](undefined);
                }
                _lastHoverEquipId = undefined;
                return;
            }

            let equip = null;
            if (pickedFeature.getProperty) {
                let id = pickedFeature.getProperty('name');
                equip = _this.equipModelMap.getEquipByModelId(id);
            } else if (equip == null) {
                equip = _this.subMobileModel.getEquip(pickedFeature.id);
            }

            if (!equip) {
                for (let i = 0, ii = _equipHoverEvents.length; i < ii; i++) {
                    _equipHoverEvents[i](undefined);
                }
                _lastHoverEquipId = undefined;
                return;
            }

            _this.nameOverlay.show(movement.endPosition.y, movement.endPosition.x, equip.equipName || equip.name);

            let d3ModelIds = _this.equipModelMap.getModelIdsByEquipId(equip.id);
            if (!d3ModelIds || d3ModelIds.length < 1) {
                for (let i = 0, ii = _equipHoverEvents.length; i < ii; i++) {
                    _equipHoverEvents[i](undefined);
                }
                _lastHoverEquipId = undefined;
                return;
            }

            // 高亮选中的对象.
            if (!_this.subModel.featureTable.isSelect(pickedFeature)) {
                _this.subModel.featureTable.silhouette(d3ModelIds, true);
            }

            if (equip.id !== _lastHoverEquipId && _equipHoverEvents.length > 0) {
                let e = {
                    equip: equip,
                    x: movement.endPosition.x,
                    y: movement.endPosition.y,
                };
                for (let i = 0, ii = _equipHoverEvents.length; i < ii; i++) {
                    _equipHoverEvents[i](e);
                }
                _lastHoverEquipId = equip.id;
            }

        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
    }


    this.zoomToZone = function(zoneCode) {
        _this.subModel.toView(zoneCode);
    };

    this.getZoneCameraId = function(zoneCode) {
        if (_this.subModel.d3ModelProp && _this.subModel.d3ModelProp.zoneCamera) {
            let cameraId = _this.subModel.d3ModelProp.zoneCamera[zoneCode];
            return cameraId;
        }

        return null;
    };
};