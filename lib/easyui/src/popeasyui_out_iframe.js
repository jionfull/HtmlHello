/**
 * 弹出easyUI通用弹出框
 * options: 具体参看sy.showEasyUIIframe函数的方法说明
 * isTop(可选项) : true|false 是否在顶级页面弹出对话框，默认为false。
 */
sy.openEasyUIDialog = function(options) {

    // 获取上一级页面的window，并传递该参数，主要用于获取该window所在弹出框的title名称，供弹出框导航窗口使用。
    options.parentWindow = window;

    if (!!options.isTop) {
        return topFrameContainer.sy.showEasyUIIframe(options);
    } else {
        // 在指定的frame范围内弹出窗体
        return businessFrameContainer.sy.showEasyUIIframe(options);
    }
}

/**
 * 关闭顶级window的easyUI通用弹出框
 */
sy.closeTopEasyUIDialog = function() {

    var thatArgs = arguments,
        parentContainer = topFrameContainer;

    // 查找当前窗体所在的dialog窗体，并关闭该dialog窗体。
    var popId = null;
    $(parentContainer.document).find("div[id^='easyPopupDiv']").each(function() {

        var easyPopupWinIframe = $(this).find("iframe[name^='easyPopupWinIframe']")[0];
        // 嵌套查找所有的子IFRAME,并包括本身
        allIfrs = [];
        sy.findAllIfr(easyPopupWinIframe, allIfrs);
        allIfrs[allIfrs.length] = easyPopupWinIframe;

        for (var i = (allIfrs.length - 1); i >= 0; i--) {
            if (document === allIfrs[i].contentWindow.document) {

                popId = $(this).attr("id");
                // 设置关闭参数，比如success，fail等，主页面根据该参数做判断。
                //parentContainer.$("#" + popId).data("closeArgs", thatArgs);
                //parentContainer.$("#" + popId).dialog('close');
                return false;
            }
        }
        return true;
    });
    return popId;
}

/**
 * 关闭工作区的easyUI通用弹出框，先在顶级window查找有没有，如果没有在工作区中查找。
 */
sy.closeEasyUIDialog = function() {

    var thatArgs = arguments;
    var jsonData = sy.findPopIdInEasyUIDialog(thatArgs);
    if (!jsonData || !jsonData.popId) return;

    // 设置关闭参数，比如success，fail等，主页面根据该参数做判断。
    jsonData.container.$("#" + jsonData.popId).data("closeArgs", thatArgs);
    jsonData.container.$("#" + jsonData.popId).dialog('close');
}

/**
 * 动态改变dialog中的title名称
 */
sy.changeTitleInEasyUIDialog = function(titleNm) {

    var thatArgs = arguments;
    var jsonData = sy.findPopIdInEasyUIDialog(thatArgs);
    if (!jsonData || !jsonData.popId) return;

    // 动态更改Title
    jsonData.container.$("#" + jsonData.popId).dialog("setTitle", titleNm);
}

/**
 * 查找弹出对话框中的动态生成的属性ID。
 */
sy.findPopIdInEasyUIDialog = function() {

    var thatArgs = arguments;
    // 1、顶级window中查找
    var popId = sy.closeTopEasyUIDialog(thatArgs);
    if (!!popId) return {
        "container": topFrameContainer,
        "popId": popId
    }; // 先在顶级window查找有没有，有就关闭返回

    // 2、工作区window中查找
    var parentContainer = businessFrameContainer;

    // 查找当前窗体所在的dialog窗体，并关闭该dialog窗体。
    $(parentContainer.document).find("div[id^='easyPopupDiv']").each(function() {

        var easyPopupWinIframe = $(this).find("iframe[name^='easyPopupWinIframe']")[0];

        // 嵌套查找所有的子IFRAME,并包括本身
        allIfrs = [];

        sy.findAllIfr(easyPopupWinIframe, allIfrs);
        allIfrs[allIfrs.length] = easyPopupWinIframe;
        for (var i = (allIfrs.length - 1); i >= 0; i--) {

            if (document === allIfrs[i].contentWindow.document) {
                popId = $(this).attr("id");
                // 设置关闭参数，比如success，fail等，主页面根据该参数做判断。
                //parentContainer.$("#" + popId).data("closeArgs", thatArgs);
                //parentContainer.$("#" + popId).dialog('close');
                return false;
            }
        }
        return true;
    });

    return {
        "container": businessFrameContainer,
        "popId": popId
    };
}

// /**
// * 由上而下获取popid
// */
// sy.getDocumentPopId=function(popIds,ifs){
// 	if(!ifs){
// 		ifs = window.top.document.getElementsByTagName('iframe');
// 	}else{
// 		ifs = ifs.contentWindow.document.getElementsByTagName('iframe');
// 	}
// 	if(ifs && ifs.length>0){

// 	 	for(var i= (ifs.length-1); i>=0; i--) {

// 		        if(document === ifs[i].contentWindow.document) {
// 		        	businessFrameContainer =
// 		        	popIds[0] = $(ifs[i]).attr("name");
// 		        	break;
// 		        }
// 		        sy.getDocumentPopId(popIds,ifs[i]);
// 		}
// 	}
// }


/**
 * 嵌套查找所有的IFRAME
 * @param ifr
 * @param allIfrs
 */
sy.findAllIfr = function(ifr, allIfrs) {
    var ct = ifr;
    if (ifr != top)
        ct = ifr.contentWindow;
    try {
        if (!!ct) {
            var ifrs = ct.document.getElementsByTagName("iframe");
            for (var i = 0; i < ifrs.length; i++) {
                allIfrs[allIfrs.length] = ifrs[i];
                sy.findAllIfr(ifrs[i], allIfrs);
            }
        }
    } catch (e) {}
}

/**
 * iframe 在IE下面有内存泄露，因此需要再关闭对话框是该元素的src属性值修改为”abort:blank”。
 * 并手工将其从DOM树上移除，然后把脚本中引用它的变量置空并调用CollectGarbage()就可以避免iframe不能正常回收所造成的内存泄露
 */
sy.destroyIframeOfDialog = function(easyPopupWinIframe, notContain) {

    var allIfrs = (!!notContain) ? [] : [easyPopupWinIframe];
    sy.findAllIfr(easyPopupWinIframe, allIfrs);

    var length = allIfrs.length;
    for (var i = (length - 1); i >= 0; i--) {
        var frame = $(allIfrs[i]);
        frame.attr('src', 'about:blank');
        //frame[0].contentWindow.document.write('');//清空iframe的内容
        frame[0].src = "javascript:false";
        frame[0].contentWindow.close(); //避免iframe内存泄漏
        frame.remove(); //删除iframe
    }
    if (typeof(CollectGarbage) == 'function') {
        CollectGarbage();
    }
}

/**
 * 弹出dialog窗体
 * option参数：支持easyUI的dialog插件的所有参数，
 * 并且扩展了contentUrl地址参数和callback函数
 * navigate(可选项)   ://title是否以导航的方式显示 true|false,默认为false
 * parentTitle(可选项)://[navigate: true]时有效，默认为本页面的title
 * extraInfo(可选项)://处理extraInfo对象，即没有后台逻辑，只用后台中转的数据。比如明细页面要看到主页面的信息。改参数传递使用post方法。
 * contentUrl(必选项) : easyUI的dialog中要显示内容的http地址
 * callback(可选项)   : 页面关闭前调用的回调函数。
 */
sy.showEasyUIIframe = function(options) {

    var randomId = sy.random4(),
        easyPopupDivId = 'easyPopupDiv' + randomId,
        easyPopupWinIframe = 'easyPopupWinIframe' + randomId,
        easyPopupDiv = $('<div></div>');

    easyPopupDiv.attr('id', easyPopupDivId);
    easyPopupDiv.css("overflow", "hidden");
    // 1、div 中内置一个iframe
    $("<iframe scrolling='auto' name='" + easyPopupWinIframe + "' frameborder='0' " +
        "src='' scrolling='no' style='width: 100%; height: 100%;'></iframe>").appendTo(easyPopupDiv);
    // 2、如果有post数据，则需要创建一个post的form表单
    var formHtmlTag = null;
    if (!!options.extraInfo) {
        formHtmlTag = sy.toBuildFormTag(options, easyPopupWinIframe);
        if (!!formHtmlTag) $(formHtmlTag).appendTo(easyPopupDiv);
    }
    // 3、添加到body中
    easyPopupDiv.appendTo('body');

    easyPopupWinIframe = easyPopupDiv.find('[name^=easyPopupWinIframe]')[0];

    // 添加导航AA->bb->cc
    if (options.navigate === true) {
        var parentTitle = (!!options.parentTitle) ? options.parentTitle : sy.getParentPopWindowTitle(options);
        options.title = (!!options.title) ? (parentTitle + "->" + options.title) : parentTitle;
    }

    var allOptions = $.extend({}, {

            cache: false,
            onBeforeOpen: function() {
                // 如果有post参数，则按照post方式提交数据，否则直接指定iframe的src
                if (!!options.extraInfo && !!formHtmlTag) {
                    formHtmlTag.submit();
                } else {
                    easyPopupWinIframe.src = options.contentUrl;
                }
            },
            onBeforeClose: function() {

                if (options.callback) {
                    // 加入try catch 防止回调函数出现异常，页面不能够关闭
                    try {
                        var callback = options.callback,
                            userParameter = $("#" + easyPopupDivId).data("closeArgs");
                        if (userParameter) {
                            if (userParameter.length == 1) {
                                callback(easyPopupWinIframe.contentWindow.document, userParameter[0]);
                            } else {
                                callback(easyPopupWinIframe.contentWindow.document, userParameter);
                            }
                        } else {
                            try {
                                callback(easyPopupWinIframe.contentWindow.document);
                            } catch (e) {
                                callback(null);
                            }
                        }

                        // 解决Iframe内存泄露
                        sy.destroyIframeOfDialog(easyPopupWinIframe);
                    } catch (e) {
                        console.info(e.message)
                    }
                }
                $("#" + easyPopupDivId).removeData("closeArgs");
            },
            onClose: function() {
                $("#" + easyPopupDivId).dialog('destroy'); //销毁dialog对象
            }
        },
        options);

    $("#" + easyPopupDivId).show().dialog(allOptions).dialog('open');

    return $("#" + easyPopupDivId);
}

/**
 * 生成Form标签及提交数据的内容
 * @param options 参数项
 * @param iframeName 提交的iframe内容
 */
sy.toBuildFormTag = function(options, iframeName) {

    var postFormHtml = $("<FORM ENCTYPE='multipart/form-data'" +
        " ACTION='" + options.contentUrl + "' METHOD='POST' target='" + iframeName + "'></FORM>");

    $.each(options.extraInfo, function(postName, postValue) {
        $("<input type='text' name='extraInfo_" + postName + "' value='" + postValue + "' />").appendTo(postFormHtml)
    });

    return postFormHtml;
}

/**
 * 获取上级弹出easyui的dialog窗口title名称，如果获取不到，则使用html的title标题
 */
sy.getParentPopWindowTitle = function(options) {

    var parentTitle = "",
        parentIframe = sy.findIframeByDocument(options);

    if (parentIframe) {
        parentTitle = $("#" + $(parentIframe).parent().attr('id')).panel('options').title;
    } else {
        parentTitle = document.title;
    }

    return parentTitle;
}

/**
 * 根据传入的document查找该document所在的iframe
 * @param curDocument
 */
sy.findIframeByDocument = function(options) {

    var curIframe = null,
        parentContainer = null,
        curDocument = options.parentWindow.document;

    if (!!options.isTop) {
        parentContainer = topFrameContainer;
    } else {
        parentContainer = businessFrameContainer;
    }

    if (curDocument === document) return curIframe;

    // 查找当前窗体所在的dialog窗体，并关闭该dialog窗体。
    $(parentContainer.document).find("div[id^='easyPopupDiv']").each(function() {

        var easyPopupWinIframe = $(this).find("iframe[name^='easyPopupWinIframe']")[0];
        if (curDocument === easyPopupWinIframe.contentWindow.document) {
            curIframe = easyPopupWinIframe;
        } else {
            $(easyPopupWinIframe.contentWindow.document).find("iframe").each(function() {
                if (curDocument === this.contentWindow.document) {
                    curIframe = easyPopupWinIframe;
                    return;
                }
            });
        }

        if (curIframe) return;
    });

    return curIframe;
}

/**
 * 函数表达式，直接调用获取可以弹出dialog的最大的窗体
 */
var topFrameContainer = window; // 实际上的顶级窗体
var businessFrameContainer = window; // 业务上的顶级窗体，比如很多系统都要求只是在工作区的iframe中弹出 
(function() {
    var flag = false;
    while (true) {
        try {
            if (topFrameContainer.self == window.top) {
                break;
            }
            // 如果有跨域的iframe，那就截止到该级的前一级，因为无法在跨域页面上弹出弹出框。
            topFrameContainer.parent.pop;
            topFrameContainer = topFrameContainer.parent;
        } catch (e) {
            flag = true;
            console.info(e.message);
            break;
        }
    }

    if (!flag) {
        // 查找页面中的工作区下一级的iframe
        var centreFrame = $(topFrameContainer.document).find("iframe[id='tab-welcome-page']");
        if (!!centreFrame && centreFrame.length > 0) {

            // 查找页面跳转的iframe
            var homeCenterFrame = $(centreFrame[0].contentWindow.document).find("iframe[id='mainIframeID']");
            if (!!homeCenterFrame && homeCenterFrame.length > 0) {
                // 将弹出页面的范围设置到工作区中
                businessFrameContainer = homeCenterFrame[0].contentWindow;
            } else {
                //将弹出页面的范围设置为工作区下一级的iframe
                businessFrameContainer = centreFrame[0].contentWindow;
            }
        }
    }

})();

/**
 * 各个浏览器中兼容console的解决办法
 */
(function() {
    //创建空console对象，避免JS报错
    if (!window.console)
        window.console = {};
    var console = window.console;

    var funcs = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml',
        'error', 'exception', 'group', 'groupCollapsed', 'groupEnd',
        'info', 'log', 'markTimeline', 'profile', 'profileEnd',
        'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'
    ];
    for (var i = 0, l = funcs.length; i < l; i++) {
        var func = funcs[i];
        if (!console[func])
            console[func] = function() {};
    }
    if (!console.memory)
        console.memory = {};
})();