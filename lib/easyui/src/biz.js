/**
 * 包含easyui的扩展和常用的方法
 */
var sy = $.extend({}, sy); /* 全局对象 */
/**
 * 生成UUID
 */
sy.random4 = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};
sy.UUID = function() {
    return (sy.random4() + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + sy.random4() + sy.random4());
};

$.fn.panel.defaults.onBeforeDestroy = function() { /* tab关闭时回收内存 */
    var frame = $('iframe', this);
    try {
        if (frame.length > 0) {
            frame[0].contentWindow.document.write('');
            frame[0].contentWindow.close();
            frame.remove();
            if ($.browser.msie) {
                CollectGarbage();
            }
        } else {
            $(this).find('.combo-f').each(function() {
                var panel = $(this).data().combo.panel;
                panel.panel('destroy');
            });
        }
    } catch (e) {}
};

$.fn.panel.defaults.loadingMessage = '数据加载中，请等待....';
$.fn.datagrid.defaults.loadMsg = '数据加载中，请等待....';

var easyuiErrorFunction = function(XMLHttpRequest) {
    //$.messager.alert('error', XMLHttpRequest.responseText.split('<script')[0]);
};
$.fn.datagrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.treegrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.combogrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.combobox.defaults.onLoadError = easyuiErrorFunction;
$.fn.form.defaults.onLoadError = easyuiErrorFunction;

var easyuiPanelOnMove = function(left, top) { /* 防止超出浏览器边界 */
    if (left < 0) {
        $(this).window('move', {
            left: 1
        });
    }
    if (top < 0) {
        $(this).window('move', {
            top: 1
        });
    }
};
$.fn.panel.defaults.onMove = easyuiPanelOnMove;
$.fn.window.defaults.onMove = easyuiPanelOnMove;
$.fn.dialog.defaults.onMove = easyuiPanelOnMove;

/* 验证文件后缀名,后缀支持多个，以逗号间隔 */
$.extend($.fn.validatebox.defaults.rules, {
    fileSuffix: {
        validator: function(fileNm, param) {
            var fileSuffixArr = param[0].split(","),
                allowArr = [],
                notAllowArr = [];

            $.each(fileSuffixArr, function(index, record) {
                if (!!record) {
                    var captital = record.slice(0, 1);
                    if (captital === "!") {
                        notAllowArr.push(record.substring(1));
                    } else {
                        allowArr.push(record);
                    }
                }
                return true;
            });
            var fileSuffixsReg = ".*\\.(" + allowArr.join("|") + ")",
                notFileSuffixsReg = ".*\\.(" + notAllowArr.join("|") + ")";

            var validFlg = (fileNm.match(new RegExp(fileSuffixsReg, 'i'))) ? true : false;
            if (notAllowArr.length > 0) {
                validFlg = validFlg && ((fileNm.match(new RegExp(notFileSuffixsReg, 'i'))) ? false : true);
            }

            return validFlg;
        },
        message: '文件后缀格式要求必须为{0}.'
    }
});


/********************************************************************
 ** 扩展 EasyUI 验证
 ** @创建人: zhuq
 ** @创建日期: 2015年04月08日
 ********************************************************************/
$.extend($.fn.validatebox.defaults.rules, {
    multiple: { //多重规则验证
        validator: function(value, vtypes) {
            var returnFlag = true;
            var opts = $.fn.validatebox.defaults;
            for (var i = 0; i < vtypes.length; i++) {
                var methodinfo = /([a-zA-Z_]+)(.*)/.exec(vtypes[i]);
                var rule = opts.rules[methodinfo[1]];
                if (value && rule) {
                    var parame = eval(methodinfo[2]);
                    if (!rule["validator"](value, parame)) {
                        returnFlag = false;
                        this.message = rule.message;
                        break;
                    }
                }
            }
            return returnFlag;
        }
    },
    CHS: {
        validator: function(value) {
            return /^[\u0391-\uFFE5]+$/.test(value);
        },
        message: '请输入汉字'
    },
    english: { // 验证英语
        validator: function(value) {
            return /^[A-Za-z]+$/i.test(value);
        },
        message: '请输入英文'
    },
    ip: { // 验证IP地址
        validator: function(value) {
            return /\d+\.\d+\.\d+\.\d+/.test(value);
        },
        message: 'IP地址格式不正确'
    },
    ZIP: {
        validator: function(value) {
            return /^[0-9]\d{5}$/.test(value);
        },
        message: '邮政编码不存在'
    },
    QQ: {
        validator: function(value) {
            return /^[1-9]\d{4,10}$/.test(value);
        },
        message: 'QQ号码不正确'
    },
    mobile: {
        validator: function(value) {
            return /^(?:13\d|15\d|18\d)-?\d{5}(\d{3}|\*{3})$/.test(value);
        },
        message: '手机号码不正确'
    },
    tel: {
        validator: function(value) {
            return /^(\d{3}-|\d{4}-)?(\d{8}|\d{7})?(-\d{1,6})?$/.test(value);
        },
        message: '电话号码不正确'
    },
    mobileAndTel: {
        validator: function(value) {
            return /(^([0\+]\d{2,3})\d{3,4}\-\d{3,8}$)|(^([0\+]\d{2,3})\d{3,4}\d{3,8}$)|(^([0\+]\d{2,3}){0,1}13\d{9}$)|(^\d{3,4}\d{3,8}$)|(^\d{3,4}\-\d{3,8}$)/
            .test(value);
        },
        message: '请正确输入电话号码'
    },
    number: {
        validator: function(value) {
            return /^[0-9]+.?[0-9]*$/.test(value);
        },
        message: '请输入数字'
    },
    money: {
        validator: function(value) {
            return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(value);
        },
        message: '请输入正确的金额'
    },
    mone: {
        validator: function(value) {
            return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(value);
        },
        message: '请输入数字且保留两位小数'
    },
    integer: {
        validator: function(value) {
            return /^[+]?[1-9]\d*$/.test(value);
        },
        message: '请输入最小为1的整数'
    },
    integ: {
        validator: function(value) {
            return /^[+]?[0-9]\d*$/.test(value);
        },
        message: '请输入整数'
    },
    year: {
        validator: function(value) {
            return /^([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})$/.test(value);
        },
        message: '请正确输入年份'
    },
    yearMonth: {
        validator: function(value) {
            return /^(\d{4}|\d{2})-(0?[1-9]|1[012])$/.test(value);
        },
        message: '请正确输入年月(yyyy-MM)'
    },
    yearLength: {
        validator: function(value, param) {
            return value.length == param[0];
        },
        message: '请正确输入年份'
    },
    range: {
        validator: function(value, param) {
            if (/^(([0-9]+[\.]?[0-9]+)|[1-9])$/.test(value)) {
                return value >= param[0] && value <= param[1];
            } else {
                return false;
            }
        },
        message: '输入的数字在{0}到{1}之间'
    },
    length: {
        validator: function(value, param) {
            //$(this)[0].value=value.substr(0,param[1]);//截断文本超出长度部分
            return value.length >= param[0] && value.length <= param[1];;
        },
        message: '输入内容长度必须介于{0}和{1}之间'
    },
    minLength: {
        validator: function(value, param) {
            return value.length >= param[0];
        },
        message: '至少输入{0}个字'
    },
    maxLength: {
        validator: function(value, param) {
            //$(this)[0].value=value.substr(0,param[0]);//截断文本超出长度部分
            return value.length <= param[0];
        },
        message: '最多{0}个字'
    },
    //select即选择框的验证
    selectValid: {
        validator: function(value, param) {
            if (value == param[0]) {
                return false;
            } else {
                return true;
            }
        },
        message: '请选择'
    },
    idCode: {
        validator: function(value) {
            return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value);
        },
        message: '请输入正确的身份证号'
    },
    loginName: {
        validator: function(value) {
            return /^[\u0391-\uFFE5\w]+$/.test(value);
        },
        message: '登录名称只允许汉字、英文字母、数字及下划线。'
    },
    equalTo: {
        validator: function(value, param) {
            return value == $(param[0]).val();
        },
        message: '两次输入的字符不一至'
    },
    equalsPwd: {
        validator: function(value, param) {
            return value == $(param[0]).val();
        },
        message: '两次密码输入不一致'
    },
    englishOrNum: { // 只能输入英文和数字
        validator: function(value) {
            return /^[a-zA-Z0-9_ ]{1,}$/.test(value);
        },
        message: '请输入英文、数字、下划线或者空格'
    },
    xiaoshu: {
        validator: function(value) {
            return /^(([1-9]+)|([0-9]+\.[0-9]{1,2}))$/.test(value);
        },
        message: '最多保留两位小数！'
    },
    //    检查包含中英文字符串的长度
    lengthZHEN: {
        validator: function(value, param) {
            var len = get_strlength(value);
            return len <= param[0];
        },
        message: '最多{0}个字符'
    },
    regPassword: {
        validator: function(value, param) {
            return (/[A-Z]+/.test(value) && /[a-z]+/.test(value) && /[0-9]+/.test(value) && /\W+/.test(value) && /\S{8,16}/.test(value));
        },
        message: '密码格式错误，密码应为8-16个字符，包括大小写字母、数字、特殊字符'
    }
});

/**
 * 获取包含中英文字符串的长度（默认中文占3个字节）
 * @creator:zhuqi
 * @param str 字符串
 * @returns {number} 返回字符串长度
 */
function get_strlength(str) {
    var len = 0;
    var zhStr = str.match(/[^ -~]/g); //返回字符串中的中文
    var allLen = str.length;
    if (zhStr == null) {
        len = str.length;
    } else {
        var zhLen = zhStr.length;
        len = (allLen - zhLen) + zhLen * 3;
    }
    return len;
}

/**
 * 验证textarea最大长度，超出最大值后的字符将被截取，并给出提示
 * @creator:zhuqi
 * @param id textarea的ID
 * @param max textarea输入的最大个数
 */
function validTextAreaLength(id, max) {
    var _val = $("#" + id).val();
    if (max > 0) {
        if (_val.length > max) { //textarea的文本长度大于maxlength
            Msg.success("提示", '最大长度为' + max + '！');
            $("#" + id).val(_val.substr(0, max)); //截断textarea的文本重新赋值

        }
    }
}

$.extend($.fn.validatebox.defaults.rules, {
    comboSelected: {
        validator: function(value, param) {
            var comboId = $(this).parent().prev().attr("id");
            var positionId = $("#" + comboId).combo('getValue');
            if (value) {
                if (value == positionId) {
                    return false;
                }
            }
            return true;
        },
        message: '请必须从下拉列表中选择.'
    }
});

/**
 * 扩展DataGrid中的下拉列表
 */
$.extend($.fn.datagrid.defaults.editors, {
    combogrid: {
        init: function(container, options) {
            var input = $('<input type="text" class="datagrid-editable-input">').appendTo(container);
            input.combogrid(options);

            // 精简分页的显示信息
            if (!!options.pagination) {
                input.combogrid('grid').datagrid('getPager').pagination({
                    showPageList: false,
                    showRefresh: false,
                    displayMsg: ''
                });
            }
            return input;
        },
        destroy: function(target) {
            $(target).combogrid('destroy');
        },
        getValue: function(target) {
            return $(target).combogrid('getValue');
        },
        setValue: function(target, value) {
            $(target).combogrid('setValue', value);
        },
        resize: function(target, width) {
            $(target).combogrid('resize', width);
        }
    },
    readonlybox: {
        init: function(container, options) {
            var input = $('<input type="text" readonly="readonly" class="datagrid-editable-input">').appendTo(container);
            input.textbox(options);
            return input;
        },
        destroy: function(target) {
            $(target).textbox('destroy');
        },
        getValue: function(target) {
            return $(target).textbox('getValue');
        },
        setValue: function(target, value) {
            $(target).textbox('setValue', value);
        },
        resize: function(target, width) {
            $(target).textbox('resize', width);
        }
    },
    yearmonthbox: {
        init: function(container, options) {
            var input = $('<input class="easyui-yearmonthbox">').appendTo(container);
            input.yearmonthbox(options);
            return input;
        },
        destroy: function(target) {
            $(target).yearmonthbox('destroy');
        },
        getValue: function(target) {
            return $(target).yearmonthbox('getValue');
        },
        setValue: function(target, value) {
            $(target).yearmonthbox('setValue', value);
        },
        resize: function(target, width) {
            $(target).yearmonthbox('resize', width);
        }
    }
});

/**
 * 获得项目根路径
 * 
 * 使用方法：sy.bp();
 */
sy.bp = function() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPaht = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
};

/**
 * 增加formatString功能
 * 
 * 使用方法：sy.fs('字符串{0}字符串{1}字符串','第一个变量','第二个变量');
 */
sy.fs = function(str) {
    for (var i = 0; i < arguments.length - 1; i++) {
        str = str.replace("{" + i + "}", arguments[i + 1]);
    }
    return str;
};

/**
 * 增加命名空间功能
 * 
 * 使用方法：sy.ns('jQuery.bbb.ccc','jQuery.eee.fff');
 */
sy.ns = function() {
    var o = {},
        d;
    for (var i = 0; i < arguments.length; i++) {
        d = arguments[i].split(".");
        o = window[d[0]] = window[d[0]] || {};
        for (var k = 0; k < d.slice(1).length; k++) {
            o = o[d[k + 1]] = o[d[k + 1]] || {};
        }
    }
    return o;
};

/**
 * 获得URL参数
 */
sy.getUrlParam = function(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
};

sy.getList = function(value) {
    if (value) {
        var values = [];
        var t = value.split(',');
        for (var i = 0; i < t.length; i++) {
            values.push('' + t[i]); /* 避免他将ID当成数字 */
        }
        return values;
    } else {
        return [];
    }
};

sy.png = function() {
    var imgArr = document.getElementsByTagName("IMG");
    for (var i = 0; i < imgArr.length; i++) {
        if (imgArr[i].src.toLowerCase().lastIndexOf(".png") != -1) {
            imgArr[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgArr[i].src + "', sizingMethod='auto')";
            imgArr[i].src = "images/blank.gif";
        }
        if (imgArr[i].currentStyle.backgroundImage.lastIndexOf(".png") != -1) {
            var img = imgArr[i].currentStyle.backgroundImage.substring(5, imgArr[i].currentStyle.backgroundImage.length - 2);
            imgArr[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='crop')";
            imgArr[i].style.backgroundImage = "url('images/blank.gif')";
        }
    }
};
sy.bgPng = function(bgElements) {
    for (var i = 0; i < bgElements.length; i++) {
        if (bgElements[i].currentStyle.backgroundImage.lastIndexOf(".png") != -1) {
            var img = bgElements[i].currentStyle.backgroundImage.substring(5, bgElements[i].currentStyle.backgroundImage.length - 2);
            bgElements[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='crop')";
            bgElements[i].style.backgroundImage = "url('images/blank.gif')";
        }
    }
};

$.ajaxSetup({
    type: 'POST',
    error: function(XMLHttpRequest, textStatus, errorThrown) { /* 扩展AJAX出现错误的提示 */
        if (XMLHttpRequest.responseText.split('<script')[0]) { //提示有值才弹出提示框
            $.messager.progress('close');
            $.messager.alert('error', XMLHttpRequest.responseText.split('<script')[0]);
        }

    }
});

sy.accessDenied = function(d) {
    try {
        if (d) {
            var data = d.split('<script')[0];
            var jsonObj = $.parseJSON(data);
            if (jsonObj.access_denied) {
                $.messager.show({
                    msg: jsonObj.message,
                    title: 'Tip'
                });
                return true;
            }
        }
    } catch (e) {}
    return false;
}

sy.trimVal = function(obj) {
    obj.value = $.trim(obj.value)
}

/**
 * 将逗号分割转化为html标签的BR分割。
 */
sy.parseDotToBrTag = function(values) {
    var valArr = values.split(",");
    var valueBrStr = "";
    $.each(valArr, function(n, value) {
        if (n == (valArr.length - 1)) {
            valueBrStr += value;
        } else {
            valueBrStr += value + "<br/>";
        }
    });
    return valueBrStr;
}

/**
 * 组织POP冒泡事件
 */
sy.cancelBubble = function(e) {
    if (e && e.stopPropagation) { //非IE   
        e.stopPropagation();
    } else { //IE   
        window.event.cancelBubble = true;
    }
}

//调用：
//	var obj={name:'tom','class':{className:'class1'},classMates:[{name:'lily'}]};
//	parseParam(obj);
//	结果："name=tom&class.className=class1&classMates[0].name=lily"
//	parseParam(obj,'stu');
//	结果："stu.name=tom&stu.class.className=class1&stu.classMates[0].name=lily"
sy.parseParam = function(param, key) {
    var paramStr = "";
    if (param instanceof String || param instanceof Number || param instanceof Boolean) {
        paramStr += "&" + key + "=" + encodeURIComponent(param);
    } else {
        $.each(param, function(i) {
            var k = key == null ? i : key + (param instanceof Array ? "[" + i + "]" : "." + i);
            paramStr += '&' + sy.parseParam(this, k);
        });
    }
    return paramStr.substr(1);
};

//html转义
sy.myHTMLEnCode = function myHTMLEnCode(str) {
    var s = "";
    if (str.length == 0) return "";
    s = str.replace(/&/g, "&amp;");
    s = s.replace(/</g, "&lt;");
    s = s.replace(/>/g, "&gt;");
    s = s.replace(/ /g, "&nbsp;");
    s = s.replace(/\'/g, "&#39;");
    s = s.replace(/\"/g, "&quot;");
    s = s.replace(/\n/g, "<br>");
    return s;
};

sy.myHTMLDeCode = function myHTMLDeCode(str) {
    var s = "";
    if (str.length == 0) return "";
    s = str.replace(/&amp;/g, "&");
    s = s.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, " ");
    s = s.replace(/&#39;/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    s = s.replace(/<br>/g, "\n");
    return s;
};

// 去除字符串中所有空格
sy.removeAllSpace = function(str) {
    return str.replace(/\s+/g, "");
};

/**
 * 生成UUID
 */
sy.random4 = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};
sy.UUID = function() {
    return (sy.random4() + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + "-" + sy.random4() + sy.random4() + sy.random4());
};

$.extend($.fn.validatebox.defaults.rules, {
    km: {
        validator: function(value, param) {
            try {

                if (!value) return true;

                var kmRegx = /^[kK][0-9]+\+[0-9]{1,3}(\.[0-9]{1,3})?$/g;

                var kmstr = value.replace(/ /g, "");

                var value = kmstr.match(kmRegx);
                if (value == null) {
                    return false;
                }

                return true;
            } catch (e) {
                alert(e);
            }

            return true;
        },
        message: '公里标格式不正确，格式参照K123+456.'
    }
});

/**
 * From扩展
 * getData 获取数据接口
 * 
 * @param {Object} jq
 * @param {Object} params 设置为true的话，会把string型"true"和"false"字符串值转化为boolean型。
 */
$.extend($.fn.form.methods, {
    getData: function(jq, params) {
        var formArray = jq.serializeArray();
        var oRet = {};
        for (var i in formArray) {
            if (typeof(oRet[formArray[i].name]) == 'undefined') {
                if (params) {
                    oRet[formArray[i].name] = (formArray[i].value == "true" || formArray[i].value == "false") ? formArray[i].value == "true" :
                        formArray[i].value;
                } else {
                    oRet[formArray[i].name] = formArray[i].value;
                }
            } else {
                if (params) {
                    oRet[formArray[i].name] = (formArray[i].value == "true" || formArray[i].value == "false") ? formArray[i].value == "true" :
                        formArray[i].value;
                } else {
                    oRet[formArray[i].name] += "," + formArray[i].value;
                }
            }
        }
        return oRet;
    }
});

/**
 * EasyUI DataGrid根据字段动态合并单元格
 * 参数 tableID 要合并table的id
 * 参数 colList 要合并的列,用逗号分隔(例如："name,department,office");
 */
function mergeCellsByField(tableID, colList, colListChild) {
    var ColArray = colList.split(",");
    var tTable = $("#" + tableID);
    var TableRowCnts = tTable.datagrid("getRows").length;
    var tmpA;
    var tmpB;
    var PerTxt = "";
    var CurTxt = "";
    for (j = ColArray.length - 1; j >= 0; j--) {
        PerTxt = "";
        tmpA = 1;
        tmpB = 0;
        for (i = 0; i <= TableRowCnts; i++) {
            if (i == TableRowCnts) {
                CurTxt = "";
            } else {
                CurTxt = tTable.datagrid("getRows")[i][ColArray[j]];
            }
            if (PerTxt == CurTxt) {
                tmpA += 1;
            } else {
                tmpB += tmpA;
                tTable.datagrid("mergeCells", {
                    index: i - tmpA,
                    field: ColArray[j], //合并字段
                    rowspan: tmpA,
                    colspan: null
                })
                if (colListChild && colListChild.length != 0) {
                    $.each(colListChild, function(k, m) {
                        tTable.datagrid("mergeCells", {
                            index: i - tmpA,
                            field: m, //合并字段
                            rowspan: tmpA,
                            colspan: null
                        });
                    })
                }
                tmpA = 1;
            }
            PerTxt = CurTxt;
        }
    }
}

/**
 * 单击行编辑时重新合并单元格
 * @param tableId 合并表ID
 * @param index 编辑行索引
 * @param col 合并列字段
 */
function remergeCell(tableId, index, colList) {
    var ColArray = colList.split(",");
    var tTable = $("#" + tableId);
    $.each(ColArray, function(j, col) {
        var TableRowCnts = tTable.datagrid("getRows").length;
        var PerTxt = tTable.datagrid("getRows")[index][col];
        var CurTxt = "";
        var tmpA = 1;
        var tmpB = 1;

        for (i = index + 1; i < TableRowCnts; i++) {
            CurTxt = tTable.datagrid("getRows")[i][col];
            if (PerTxt == CurTxt) {
                tmpA += 1;
            } else {
                break;
            }
        }

        if (index > 0) {
            for (i = index - 1; i < index; i--) {
                if (i < 0) break;
                CurTxt = tTable.datagrid("getRows")[i][col];
                if (PerTxt == CurTxt) {
                    tmpB += 1;
                } else {
                    break;
                }
            }
            if (tmpB > 1) {
                tTable.datagrid("mergeCells", {
                    index: index - tmpB + 1,
                    field: col, //合并字段
                    rowspan: tmpB,
                    colspan: null
                })
            }
            var tmpC = 1;
            var PerTxtPre = tTable.datagrid("getRows")[index - 1][col];
            for (i = index - 2; i < index; i--) {
                if (i < 0) break;
                CurTxt = tTable.datagrid("getRows")[i][col];
                if (PerTxtPre == CurTxt) {
                    tmpC += 1;
                } else {
                    break;
                }
            }
            if (tmpC > 1) {
                tTable.datagrid("mergeCells", {
                    index: index - tmpC,
                    field: col, //合并字段
                    rowspan: tmpC,
                    colspan: null
                })
            }
        }

        if (tmpA > 1) {
            tTable.datagrid("mergeCells", {
                index: index,
                field: col, //合并字段
                rowspan: tmpA,
                colspan: null
            })
        }


    });

}

//自定义验证规则 名称为myDate  
$.extend($.fn.validatebox.defaults.rules, {
    dateType: {
        validator: function(mytime, param) {
            // alert('mytime:'+mytime);
            //标准时间格式  
            var regStandard = /^\1|2\d{3}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}\s*$/; //匹配标准日期格式  2014-1-20 12:10:00  
            //日期快速输入格式    
            var regA = /^\1|2\d{3}-\d{1,2}-\d{1,2}-\d{1,2}-\d{1,2}-\d{1,2}\s*$/; //匹配日期 和 时\分\秒  2014-1-20-12-10-00 意思是2014-1-20 12:10:00  
            var regB = /^\1|2\d{3}-\d{1,2}-\d{1,2}-\d{1,2}-\d{1,2}\s*$/; //匹配日期 和 时\分  2014-1-20-12-10-00 意思是2014-1-20 12:10  
            var regC = /^\1|2\d{3}-\d{1,2}-\d{1,2}\s*$/; //匹配日期  2014-1-20  
            var x = "";

            var regD = /^\1|2\d{3}-\d{1,2}\s*$/; //匹配年月 2014-1


            if (!regStandard.test(mytime)) {
                if (regA.test(mytime) && 'datetime' == param[0]) {
                    var tempArr = mytime.split('-');
                    x = tempArr[0] + "-" + tempArr[1] + "-" + tempArr[2] + " " + tempArr[3] + ":" + tempArr[4] + ":" + tempArr[5];
                    /**/
                    if (!checkDateTime("date", x) || !checkDateTime("time", x)) {
                        alert(1234134);
                        $.fn.validatebox.defaults.rules.dateType.message = "日期错误！请按照以下格式输入2014-1-20 12:10:00";
                        return false;
                    }
                } else if (regB.test(mytime) && 'datehourminute' == param[0]) {
                    var tempArr = mytime.split('-');
                    x = tempArr[0] + "-" + tempArr[1] + "-" + tempArr[2] + " " + tempArr[3] + ":" + tempArr[4] + ":00";
                    /**/
                    if (!(checkDateTime("date", x) && checkDateTime("time", x))) {
                        $.fn.validatebox.defaults.rules.dateType.message = "日期错误!请输入有效日期如2014-1-20 12:10";
                        return false;
                    }
                } else if (regC.test(mytime) && 'date' == param[0]) {
                    // alert(regC.test(mytime)+','+param[0]);
                    x = mytime + " 00:00:00";
                    /**/
                    if (!checkDateTime("date", x)) {
                        $.fn.validatebox.defaults.rules.dateType.message = "日期错误!请输入有效日期如2014-1-20";
                        return false;
                    }
                } else if (regD.test(mytime) && 'yearmonth' == param[0]) {
                    x = mytime + "-01 00:00:00";
                    if (!checkDateTime("datemonth", x)) {
                        $.fn.validatebox.defaults.rules.dateType.message = "日期错误!请输入有效日期如2014-01";
                        return false;
                    }
                } else {
                    $.fn.validatebox.defaults.rules.dateType.message = "日期错误!请输入有效日期";
                    return false;
                }
            } else {
                if (!(checkDateTime("date", mytime) && checkDateTime("time", mytime))) {
                    $.fn.validatebox.defaults.rules.dateType.message = "日期错误!请输入有效日期如2014-1-20 12:10:00";
                    return false;
                }
            }
            return true;
        },
        message: ''
    }
});

function checkDateTime(type, datetime, split) {
    var date = datetime.split(" ")[0];
    var time = datetime.split(" ")[1];
    //alert(date + '\n' + time)  
    switch (type) {
        case "time": //检查时分秒的有效性  
            var tempArr = time.split(":");
            if (parseInt(tempArr[0]) > 23) {
                return false;
            }
            if (parseInt(tempArr[1]) >= 60 || parseInt(tempArr[2]) >= 60) {
                return false;
            }
            break;
        case "date": //检查日期的有效性  
            var tempArr = date.split("-");
            if (parseInt(tempArr[1]) == 0 || parseInt(tempArr[1]) > 12) { //月份  
                return false;
            }
            var lastday = new Date(parseInt(tempArr[0]), parseInt(tempArr[1]), 0); //获取当月的最后一天日期           
            if (parseInt(tempArr[2]) == 0 || parseInt(tempArr[2]) > lastday.getDate()) { //当月的日  
                return false;
            }
            var myDate = new Date(parseInt(tempArr[0]), parseInt(tempArr[1]) - 1, parseInt(tempArr[2]));
            if (myDate == "Invalid Date") {
                return false;
            }
            break;
        case "datemonth": //检查日期年月的有效性
            var tempArr = date.split("-");
            if (parseInt(tempArr[0]) == 0 || parseInt(tempArr[0]) > 2999) { //年份  
                return false;
            }
            if (parseInt(tempArr[1]) == 0 || parseInt(tempArr[1]) > 12) { //月份  
                return false;
            }
            var myDatemonth = new Date(parseInt(tempArr[0]), parseInt(tempArr[1]) - 1);
            if (myDate == "Invalid Date") {
                return false;
            }
            break;
    }

    return true;
}
/**
 * easyUI datagrid按百分比显示列宽
 * @param width
 * @param percent
 * @returns {number}
 */
function fixWidthByPercent(percent, width) {
    var _contentWidth;
    if (width) {
        _contentWidth = width - 5;
    } else {
        _contentWidth = document.body.clientWidth - 5;
    }
    return _contentWidth * percent;
}

/**
 * 将easyui的校验规则转换为下拉列表的数据
 */
sy.validTypeJson = (function() {
    var validTypeJson = [],
        rulesJson = $.fn.validatebox.defaults.rules;
    for (var key in rulesJson) {
        validTypeJson.push({
            rule: key,
            message: rulesJson[key].message
        });
    }

    return validTypeJson;
})();

//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg); //匹配目标参数
    if (r != null) return unescape(r[2]);
    return null; //返回参数值
}

/**
 * 将datagrid中的行记录转换为json格式的字符串。
 * @param dataGrid
 * @param rowId
 * @returns
 */
sy.toRowJsonByRowId = function(dataGrid, rowId) {

    var rowIndex = dataGrid.datagrid('getRowIndex', rowId); //id是关键字值
    var operatedRow = dataGrid.datagrid('getData').rows[rowIndex];
    if (!!operatedRow) {
        return JSON.stringify(operatedRow);
    };
    return "";
}

/**
 * 起始日期大小判断
 * @param dataGrid
 * @param rowId
 * @returns
 */
sy.checkBeginAndEndDate = function(startDate, endDate) {
    if (!startDate || !endDate) {
        return true;
    }

    var beforDate = new Date(startDate),
        afterDate = new Date(endDate);

    if (beforDate.getTime() > afterDate.getTime()) {
        return false;
    }
    return true;
}