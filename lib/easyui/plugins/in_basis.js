$(document).ready(function() {
    window.setTimeout('cagegoin()', 2);
    var qcloud = {};
    $('[_s_nav]').hover(
        function() {
            var s_nav = $(this).attr('_s_nav');
            $('#' + s_nav).stop(true, true).show();
            var w_nav = $(window).width() - $('#' + s_nav).offset().left;
            if (w_nav < 260) {
                $('#' + s_nav).addClass("sub_menu_l");
            };
        },
        function() {
            var s_nav = $(this).attr('_s_nav');
            $('#' + s_nav).stop(true, true).hide();
        }
    );
    $('[_t_nav]').hover(function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            var n = 1;
            var i = 1;
            $('[_t_nav]').each(function() {
                n = n + 1;
            });
            $('[_t_nav]').each(function() {
                i = i + 1;
                w = $('[_t_nav]').offset().left + (i - n / 2) * 110 - 200;
                if (_nav == $(this).attr('_t_nav')) {
                    $(this).addClass("nav-up-selected");
                    $('#' + _nav).css({
                        left: w + 'px'
                    });
                } else {
                    $(this).removeClass("nav-up-selected");
                }
            });

            $('#' + _nav).stop(true, true).slideDown(200);
        }, 150);
    }, function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            $('[_t_nav]').removeClass('nav-up-selected');
            $('#' + _nav).stop(true, true).slideUp(200);
        }, 150);
    });
})

function loadhrefcage(url, time) {
    cagegoout();
    window.setTimeout("window.location.href = \"" + url + "\"", time);
}

function cagegoin() {
    $('#alltran').css({
        bottom: "0vw"
    })
}

function cagegoout() {
    $('#alltran').css({
        bottom: "100vw"
    })
}