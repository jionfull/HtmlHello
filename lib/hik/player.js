var playerWndSetting = {

};
playerWndSetting.state = false;
playerWndSetting.current = false;
/**
 * 播放器位置设置
 */
playerWndSetting.check = function() {
    if (playerWndSetting.state) {
        try {
            document.getElementById('mainIframeID').contentWindow.hideWnd();
            playerWndSetting.current = true;
        } catch (err) {
            playerWndSetting.state = false;
        }
    }
}
/**
 * 播放器位置恢复
 */
playerWndSetting.recover = function() {
    if (playerWndSetting.current) {
        document.getElementById('mainIframeID').contentWindow.showWnd();
    }
}
/**
 * 停止所有播放
 */
playerWndSetting.stop = function() {
    if (playerWndSetting.state) {
        var contentWindow = document.getElementById('mainIframeID').contentWindow;
        if (contentWindow && contentWindow.stopAllPlayback) {
            document.getElementById('mainIframeID').contentWindow.stopAllPlayback();
        }
        if (contentWindow && contentWindow.stopAllPreview) {
            document.getElementById('mainIframeID').contentWindow.stopAllPreview();
        }
    }
}
/**
 * 恢复播放(实时视频恢复)
 */
playerWndSetting.start = function() {
    if (playerWndSetting.state) {
        var index = document.getElementById('mainIframeID').contentWindow.cameraIndex;
        var arr = document.getElementById('mainIframeID').contentWindow.cameraIndexCodeArray;
        document.getElementById('mainIframeID').contentWindow.getLayout();
        setTimeout(() => {
            var wndNum = document.getElementById('mainIframeID').contentWindow.wndNum;
            for (var i = 0; i < wndNum; i++) {
                if (index < 0) {
                    index = index + 16;
                }
                if (arr[index - 1]) {
                    document.getElementById('mainIframeID').contentWindow.startPreview(arr[index - 1], 1, 0, 0, i + 1);
                }
                index--;
            }
            document.getElementById('mainIframeID').contentWindow.cameraIndexCodeArray = new Array(16);
            document.getElementById('mainIframeID').contentWindow.cameraIndex = 0;
        }, 100);
    }
}